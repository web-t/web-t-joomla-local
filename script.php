<?php

// No direct access to this file
defined( '_JEXEC' ) or die( 'Restricted access' );

use Joomla\CMS\Log\Log;

/**
 * Class that is responsible for running on package installation
 *
 * @license     GNU General Public License version 2 or later, see License.txt
 */
class pkg_webtInstallerScript {

	/**
	 * Runs right after any installation action is performed on the component.
	 *
	 * @param  string    $type   - Type of PostFlight action. Possible values are:
	 *                           - * install
	 *                           - * update
	 *                           - * discover_install
	 * @param  \stdClass $parent - Parent object calling object.
	 *
	 * @return void
	 */
	public function postflight( $type, $parent ) {
		if ( $type != 'install' ) {
			return;
		}
		$plugins = array( 'webt', 'webt_etranslation', 'languagefilter' );
		foreach ( $plugins as $pluginElement ) {
			Log::add( "Enabling plugin $pluginElement...", Log::INFO, 'webt' );
			$pluginType = 'plugin';

			$db = \Joomla\CMS\Factory::getContainer()->get( 'DatabaseDriver' );

			$query = $db->getQuery( true );

			// Fields to update.
			$fields = array(
				$db->quoteName( 'enabled' ) . ' = 1',
			);

			// On what conditions
			$conditions = array(
				$db->quoteName( 'element' ) . ' = ' . $db->quote( $pluginElement ),
				$db->quoteName( 'type' ) . ' = ' . $db->quote( $pluginType ),
			);

			$query->update( $db->quoteName( '#__extensions' ) )->set( $fields )->where( $conditions );

			$db->setQuery( $query );

			$db->execute();
			Log::add( "Enabled plugin $pluginElement", Log::INFO, 'webt' );
		}
	}
}
