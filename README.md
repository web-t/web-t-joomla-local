# WEB-T Joomla extension
Automated website translation solution for Joomla websites. This solution automatedly translates Joomla articles either on article save or page load events or using pre-translation feature. WEB-T Joomla solution is a package that consists of 4 parts: a *component*, a *content-group plugin*, a *webservice-group plugin* and a *library*. 
- The *component* allows the user to set up the MT provider (eTranslation or custom), plugin configuration and enables the user to pre-translate all created articles from the default Joomla language to the chosen installed content languages. 
- The *content-group plugin’s* main feature is to automatically translate each source article (articles with default language set as content language) on save or content prepare event. 
- The *webservice-group plugin’s* main feature is to create an HTTP endpoint which the translation responses from eTranslation will be sent to. It receives the response and redirects it to component, which processes it appropriately.
- The *library* contains all models and services that are used by the component and plugins. These models are responsible for directly interacting with the database

## Solution architecture
![Architecture](architecture.png)

## Dependencies
This plugin relies on Joomla built-in extensions
- `Languages` - solution uses installed site & content languages
- `Language Switcher` - allows the user to easily switch between languages
- `System - Language Filter` - allows the system to filter articles by language


## Setup
This package contains 4 parts: a component, a library and two plugins. They should be put into one package. To create the package, create a ZIP file from the contents of this repository.

To install the package:
1. Open Joomla administrator page and go to systems.
2. Open Install->Extensions.
3. Either drag and drop the plugin zip or choose the zip file to import the plugin.

How to use:
1. Go to `Components > WEB-T > Translation provider`.
2. Configure the machine translation provider. For eTranslation enter eTranslation API credentials; for Custom provider - specify provider's Base URL and API key. Click `Save`.
3. (Optional) Go to `Components > WEB-T > Configuration` and choose MT engine for each supported target language.
4. Go to `Components > WEB-T > Machine translation`, choose target languages you want to translate articles to and press `Translate articles`.

## License
This module is licensed under the GNU General Public License, version 2 (GPLv2) or later