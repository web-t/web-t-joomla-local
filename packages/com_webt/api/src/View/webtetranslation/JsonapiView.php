<?php

namespace Component\Webt\Api\View\Webtetranslation;

defined( '_JEXEC' ) or die;

use Joomla\CMS\MVC\View\JsonApiView as BaseApiView;

/**
 * Class responsible for handling etranslation json request responses
 *
 * @license     GNU General Public License version 2 or later, see License.txt
 */
class JsonapiView extends BaseApiView {

	/**
	 * Execute and display a template script.
	 *
	 * @param   array|null $items  Array of items.
	 *
	 * @return  string
	 *
	 * @since   4.0.0
	 */
	public function displayList( array $items = null ) {
		return null;
	}
}
