<?php
namespace Component\Webt\Api\Controller;

defined( '_JEXEC' ) or die;

use Joomla\CMS\Log\Log;
use Joomla\CMS\MVC\Controller\ApiController;
use JLoader;

JLoader::registerNamespace( 'Webt', JPATH_LIBRARIES . '/Webt' );

use Webt\Model\EtranslationResponseModel;
use Webt\Services\EtranslationUtils;

/**
 * The WebtetranslationController class is an API controller for handling eTranslation responses.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_webt
 * @license     GNU General Public License version 2 or later, see License.txt
 */
class WebtetranslationController extends ApiController {

	protected $contentType = 'Webtetranslation';

	protected $default_view = 'Webtetranslation';

	/**
	 * POST request endpoint for eTranslation responses
	 *
	 * @param   integer $recordKey  The primary key of the item (if exists).
	 *
	 * @return  integer  The record ID on success, false on failure
	 *
	 * @since   4.0.6
	 */
	public function add( $recordKey = null ) {
		$request_id = $this->input->post->get( 'id' );
		Log::add( "Received response from eTranslation [ID=$request_id]", Log::DEBUG, 'webt' );

		$model = new EtranslationResponseModel();
		$entry = $model->getTranslationEntry( $request_id );
		if ( ! $entry ) {
			Log::add( "Received translation response for non-existing request ID: $request_id", Log::ERROR, 'webt' );
			return false;
		} else {
			$error_code = $this->input->json->get( 'error_code' );
			if ( $error_code ) {
				$error_message = $this->input->json->get( 'error-message' );
				Log::add( "eTranslation response error $error_code: '$error_message' [ID=$request_id]", Log::ERROR, 'webt' );
				$model->updateTranslationEntry( $request_id, EtranslationUtils::$error_value );
				return $request_id;
			}
		}

		if ( EtranslationUtils::$initial_value === $entry->response ) {
			// Process response.
			$data = $this->input->json->getRaw();
			$model->updateTranslationEntry( $request_id, $data );
			return $request_id;
		} elseif ( EtranslationUtils::$timeout_value === $entry->response ) {
			// Process late response.
			Log::add( "Received late translation response for entry: $request_id", Log::ERROR, 'webt' );
            $data = $this->input->json->getRaw();
			$model->updateTranslationEntry( $request_id, $data );
			return $request_id;
		} else {
			Log::add( "Received translation response for already processed entry: $request_id", Log::ERROR, 'webt' );
			return false;
		}
	}
}
