<?php

defined( '_JEXEC' ) or die;

use Joomla\CMS\Uri\Uri;

/**
 * @package     Joomla.Administrator
 * @subpackage  com_webt
 *
 * @copyright
 * @license     GNU General Public License version 2 or later; see License.txt
 */

$asset_dir = Uri::root() . 'administrator/components/com_webt/assets';
$this->document->addStyleSheet( "$asset_dir/css/global.css" );
$this->document->addStyleSheet( "$asset_dir/css/about.css" );

$ec_logotype_url   = "$asset_dir/images/ec_logotype.svg";
$webt_logotype_url = "$asset_dir/images/webt_logotype.svg";

?>
<div class="card">
	<div class="card-header">
		<h3>About this extension</h3>
	</div>
	<div class="card-body">
		<div class="row desc">
			<p>WEB-T is an extension that allows you to translate your Joomla articles using machine translation. It comes with a default translation provider, eTranslation, but can be configured to use custom providers.</p>
			<p>See how to <a target="_blank" href="https://website-translation.language-tools.ec.europa.eu/solutions/joomla_en">configure and use this extension.</a><p>
			<strong>More information</strong>
			<p>WEB-T is a part of European Multilingual Web (EMW) project by EC (<a target="_blank" href="https://website-translation.language-tools.ec.europa.eu/web-t-connecting-languages-0_en">Learn more</a>).</p>
			<p>
				<img alt="EC logotype" class="logotypes" src="<?php echo $ec_logotype_url; ?>" />
				<img alt="WEB-T logotype" class="logotypes" id="webt-logotype" src="<?php echo $webt_logotype_url; ?>" />
			</p>
		</div>
	</div>
</div>
