<?php

defined( '_JEXEC' ) or die;

use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Uri\Uri;

/**
 * @package     Joomla.Administrator
 * @subpackage  com_webt
 *
 * @copyright
 * @license     GNU General Public License version 2 or later; see License.txt
 */

$wa = $this->document->getWebAssetManager();
$wa->useScript('form.validate');

$asset_dir = Uri::root() . 'administrator/components/com_webt/assets';

$this->document->addScriptOptions('com_webt', array( 'mt_engine' => $this->mtEngine ) );
$this->document->addScript( "$asset_dir/js/settings.js" );
$this->document->addStyleSheet( "$asset_dir/css/global.css" );
$this->document->addStyleSheet( "$asset_dir/css/settings.css" );

?>
<div id="webt-message-container" aria-live="polite">
    <joomla-alert type="warning" role="alert" style="animation-name: joomla-alert-fade-in;">
        <div class="alert-heading">
            <span class="warning"></span><span class="visually-hidden">Warning</span>
        </div>
        <div class="alert-wrapper">
            <div class="alert-message">Please check the MT engines you've set up under WEB-T &gt; Configuration after making changes to the MT provider</div>
        </div>
    </joomla-alert>
</div>
	<div class="card">
    <form method="post" class="form-validate" name="adminForm" id="adminForm">
		<div class="card-header">
			<h3>Translation provider settings</h3>
		</div>
            <div class="card-body">
                <div class="row desc">
			<p>Choose machine translation provider to use in website translation.</p>
		</div>
			<div class="mb-4 row">
				<div class="col-6 col-sm-4 col-md-3 col-xl-2">
					<input id="etranslation" type="radio" name="mtEngine" value="etranslation" <?php echo 'customProvider' !== $this->mtEngine ? 'checked' : ''; ?> />
					<label for="etranslation">eTranslation</label>
				</div>
				<div class="col">
					<input id="customProvider" type="radio" name="mtEngine" value="customProvider" <?php echo 'customProvider' === $this->mtEngine ? 'checked' : ''; ?> />
					<label for="customProvider">Custom provider</label>
				</div>
			</div>
            <div id="etranslationFields">
                <div class="mb-3 row">
                    <div class="col-12 col-sm-9 col-md-8 col-lg-6">
                        <label for="ApplicationName">Application name: </label>
                        <input id="ApplicationName" title="ApplicationName" type="text" name="ApplicationName" class="input form-control" required value="<?php echo $this->applicationName ?? ''; ?>">
                    </div>
                </div>
                <div class="mb-5 row">
                    <div class="col-12 col-sm-9 col-md-8 col-lg-6">
                        <label for="Password">Password: </label>
                        <input id="Password" title="Password" type="password" name="Password" class="input form-control" required value="<?php echo $this->password ?? ''; ?>">
                    </div>
                </div>                
                <div class="row">
                    <div class="col">
                        <p>Set up an account for <a href="https://website-translation.language-tools.ec.europa.eu/automated-translation_en" target="_blank">eTranslation</a></p>
                    </div>
                </div>
            </div>
            <div id="customProviderFields">
                <div class="mb-3 row">
                    <div class="col-12 col-sm-9 col-md-8 col-lg-6">
                        <label for="MTUrl">Base URL: </label>
                        <input id="MTUrl" title="MTUrl" type="text" name="MTUrl" class="input form-control" required value="<?php echo $this->MTUrl ?? ''; ?>">
                    </div>
                </div>
                <div class="mb-5 row">
                    <div class="col-12 col-sm-9 col-md-8 col-lg-6">
                        <label for="ApiKey">API Key: </label>
                        <input id="ApiKey" title="ApiKey" type="text" name="ApiKey" class="input form-control" required value="<?php echo $this->apiKey ?? ''; ?>">
                    </div>
                </div>                
                <div class="row">
                    <div class="col">
                        <p>Get <a href="https://website-translation.language-tools.ec.europa.eu/automated-translation_en" target="_blank">MT provider access</a></p>
                    </div>
                </div>
            </div>
		</div>
	<input type="hidden" name="task" value="">
	<?php echo HTMLHelper::_( 'form.token' ); ?>
</form>
</div>
