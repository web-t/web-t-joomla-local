<?php

defined( '_JEXEC' ) or die;

use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Uri\Uri;

/**
 * @package     Joomla.Administrator
 * @subpackage  com_webt
 *
 * @copyright
 * @license     GNU General Public License version 2 or later; see License.txt
 */

$wa = $this->document->getWebAssetManager();
$wa->useScript('form.validate');

$asset_dir = Uri::root() . 'administrator/components/com_webt/assets';
$this->document->addStyleSheet( "$asset_dir/css/global.css" );
$this->document->addStyleSheet( "$asset_dir/css/configuration.css" );
?>

<form method="post" class="card form-validate" name="adminForm" id="adminForm">
	<div class="card">
		<div class="card-header">
			<h3>Configuration</h3>
		</div>
		<div class="card-body">
            <div class="row desc">
                <p>Configure automated translation options and workflow.</p>
            </div>
			<div class="mb-4 row">
				<div class="col">
                    <input type="checkbox" id="inheritProperties" name="inheritProperties" <?php echo $this->inheritArticleProperties ? 'checked' : ''; ?> />
                    <label for="inheritProperties">Inherit original article properties</label>
                    <div>Inherit status & featured properties from original article when machine translating. If disabled, newly translated articles will be unpublished and unfeatured.</div>
				</div>
			</div>
            <div class="mb-4 row">
				<div class="col">
                    <input type="checkbox" id="translateOnVisit" name="translateOnVisit" <?php echo $this->translateOnVisit ? 'checked' : ''; ?> />
                    <label for="translateOnVisit">Translate on the fly</label>
                    <div>Check for article translations on page load and translate if there are any missing languages.</div>
				</div>
			</div>
			<div class="mb-4 row">
				<div class="col">
                    <input type="checkbox" id="showMtNotice" name="showMtNotice" <?php echo $this->showMtNotice ? 'checked' : ''; ?> />
                    <label for="showMtNotice">Show MT notice</label>
                    <div>Show machine translated content notice at the top of every translated article page.</div>
				</div>
			</div>
			<div class="mb-4 row <?php echo 'customProvider' === $this->mtEngine ? 'd-none' : ''; ?>">
				<div class="col-12">
					<label for="etranslationTimeout">eTranslation timeout: </label>
					<div>Time in seconds to wait on eTranslation service before declaring timeout. Infinite if zero. Affects only translation on the fly.</div>
				</div>
				<div class="col col-xs-3 col-md-2 col-xl-1">					
					<input id="etranslationTimeout" title="etranslationTimeout" type="number" name="etranslationTimeout" class="input form-control" required value="<?php echo $this->etranslationTimeout; ?>">
				</div>
			</div>
			<div class="card" id="mt-engine-table">
				<div class="card-header">
					MT engines
				</div>
				<div class="card-body">
					<p id="mt-engine-desc">Choose machine translation engines to use for each translation language.</p>
					<div class="row">
						<div class="col-12">
							<div id="j-main-container" class="j-main-container">
								<table class="table" id="domainList">
									<thead>
										<tr>
											<th scope="col" class="w-60">
												<?php echo "Language" ?>
											</th>
											<th scope="col" class="w-40">
												<?php echo "MT Engine" ?>
											</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach ($this->availableLanguages as $i => $language) : ?>
											<tr class="row<?php echo $i % 2; ?>">
												<td>
													<?php echo $language->title; ?>
												</td>
												<td>
													<?php 
														$langcode       = $language->lang_code;
														$select_id      = "engines[$langcode]"; 
														$selectedEngine = isset ( $this->selectedEngines->$langcode ) ? $this->selectedEngines->$langcode : '';
													?>
													<select aria-label="MT engine for <?php echo $language->title; ?>" name="<?php echo $select_id ?>" id="<?php echo $select_id ?>">
														<option <?php echo $selectedEngine === null ? 'selected' : ''; ?> value="">[Unspecified]</option>
														<?php foreach ($this->supportedEngines[ $langcode ] as $engine) : ?>
															<option <?php echo $engine->domain === $selectedEngine ? 'selected' : ''; ?> value="<?php echo $engine->domain ?>"><?php echo isset( $engine->name ) ? $engine->name: $engine->domain; ?></option>
														<?php endforeach; ?>
													</select>
												</td>
											</tr>
										<?php endforeach; ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<input type="hidden" name="task" value="">
	<?php echo HTMLHelper::_( 'form.token' ); ?>
</form>
