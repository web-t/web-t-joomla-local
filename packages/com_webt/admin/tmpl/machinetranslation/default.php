<?php

defined( '_JEXEC' ) or die;

use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Uri\Uri;

/**
 * @package     Joomla.Administrator
 * @subpackage  com_webt
 *
 * @copyright
 * @license     GNU General Public License version 2 or later; see License.txt
 */

$translation_progress = $this->calculateTranslationProgress();

$asset_dir = Uri::root() . 'administrator/components/com_webt/assets';
$this->document->addStyleSheet( "$asset_dir/css/global.css" );
$this->document->addStyleSheet( "$asset_dir/css/machine_translation.css" );
?>

<div class="card">
    <div class="card-header"><h3>Machine translation</h3></div>
    <div class="card-body">
        <div class="row desc">
            <p>
                Follow translation progress and translate existing content. New content will be translated automatically.<br/>
                <span class="desc-note"><strong>Note: </strong>Translating different languages separately can be more effiecient.</span>
            </p>
        </div>
        <form method="post" class="card form-validate" name="adminForm" id="adminForm">
            <div class="row">
                <div class="col-md-12">
                    <div id="j-main-container" class="j-main-container">
                        <table class="table" id="languageTable">
                            <thead>
                                <tr>
                                    <td class="w-1 text-center">
                                        <?php echo HTMLHelper::_('grid.checkall'); ?>
                                    </td>
                                    <th scope="col">
                                        <?php echo "Target language" ?>
                                    </th>
                                    <th scope="col" class="text-center">
                                        <?php echo "Translation progress" ?>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($this->availableLanguages as $i => $language) : ?>
                                    <tr class="row<?php echo $i % 2; ?>">
                                        <td class="text-center">
                                            <?php echo HTMLHelper::_('grid.id', $i, $language->lang_code, false, 'cid', 'cb', $language->title ); ?>
                                        </td>
                                        <td>
                                            <?php echo $language->title; ?>
                                        </td>
                                        <td class="text-center">
                                            <?php echo $translation_progress[ $language->lang_code ] . '%' ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                        <input type="hidden" name="task" value="">
                        <input type="hidden" name="boxchecked" value="0">
                        <?php echo HTMLHelper::_('form.token'); ?>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
