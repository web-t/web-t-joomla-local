/**
 * Function to update the visibility of form fields based on the selected engine.
 * @param {string} engine - The selected translation engine ('customProvider' or 'etranslation').
 */
function updateFormFieldVisibility(engine) {
    var customProviderFields = document.querySelector("#customProviderFields");
    var etranslationFields = document.querySelector("#etranslationFields");
    if ( engine === "customProvider" ) {
        customProviderFields.style.display = "block";
        etranslationFields.style.display = "none";
    } else {
        customProviderFields.style.display = "none";
        etranslationFields.style.display = "block";
    }
}

//Responsible for showing correct MT engine settings
document.addEventListener("DOMContentLoaded", function() {
    document.querySelectorAll('input[name="mtEngine"]').forEach((elem) => {
        elem.addEventListener("change", function(event) {
            var value = event.target.value;
            updateFormFieldVisibility( value );
            var warn = document.querySelector("#webt-message-container");
            if ( value !== Joomla.getOptions('com_webt').mt_engine ) {
                warn.style.display = "block";
            }
        });
    });
    
    updateFormFieldVisibility( Joomla.getOptions('com_webt').mt_engine );
});

//Responsible for showing warning message if MT settings are not configured
document.addEventListener("DOMContentLoaded", function() {
    document.querySelectorAll('input[type="text"]').forEach((elem) => {
        elem.addEventListener("input", function(event) {
            var warn = document.querySelector("#webt-message-container");
            warn.style.display = "block";
        });
    });
});
