function hideMtNotice() {
    document.querySelector("#mt-notice-container").style.display = "none";
    document.querySelector(".mt-notice-space").style.display = "none";
}

//Responsible for showing MT notice
document.addEventListener("DOMContentLoaded", function() {
    var text = Joomla.getOptions('webt').mt_notice_text;
    var lang = Joomla.getOptions('webt').mt_notice_lang;
    var mtNoticeContainer = document.createElement("div");
    mtNoticeContainer.classList.add("mt-notice-container");
    mtNoticeContainer.innerHTML = `
        <div id="mt-notice-container">
            <div class="translation-notice" lang="${lang}">
                ${text}
            </div>
            <div id="mt-notice-hide" onclick="hideMtNotice()" tabindex="0">&times;</div>
        </div>
        <div class="mt-notice-space"></div>
    `;
    document.body.insertBefore(mtNoticeContainer, document.body.firstChild);

    var mtNoticeContainer = document.querySelector("#mt-notice-container");
    var mtNoticeSpace = document.querySelector(".mt-notice-space");
    if (mtNoticeContainer) {
        mtNoticeSpace.style.height = mtNoticeContainer.offsetHeight + "px";
    }

    var closeButton = document.getElementById("mt-notice-hide");
    closeButton.addEventListener("keyup", function(event) {
        if (event.key === 'Enter') {
            closeButton.click();
        }
    });
});