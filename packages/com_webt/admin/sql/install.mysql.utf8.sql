CREATE TABLE IF NOT EXISTS `#__webttranslator_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL DEFAULT '',
  `value` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__webttranslator_etranslation_temp` (
  `request_id` varchar(25) NOT NULL,
  `response` longtext NOT NULL,
  `source_article` int(10) NOT NULL,
  `target_langcode` varchar(8) NOT NULL,
  PRIMARY KEY (`request_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 DEFAULT COLLATE=utf8mb4_unicode_ci;

