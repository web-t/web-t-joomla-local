<?php

namespace Component\Webt\Administrator\Model;

defined( '_JEXEC' ) or die;

use Joomla\CMS\MVC\Model\ListModel;

/**
 * Model class that is needed for etranslation request receiving
 *
 * @license     GNU General Public License version 2 or later, see License.txt
 */
class WebtetranslationModel extends ListModel {}
