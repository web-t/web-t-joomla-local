<?php

namespace Component\Webt\Administrator\Controller;

defined( '_JEXEC' ) or die;

use Joomla\CMS\Factory;
use Joomla\CMS\MVC\Controller\BaseController;

/**
 * @package     Joomla.Administrator
 * @subpackage  com_webt
 *
 * @copyright
 * @license     GNU General Public License version 2 or later; see License.txt
 */

 
/**
 * Controller responsible for about page
 */
class AboutController extends BaseController {
	/**
	 * The default view for the display method.
	 *
	 * @var string
	 */
	protected $default_view = 'About';
}
