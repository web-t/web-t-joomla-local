<?php

namespace Component\Webt\Administrator\Controller;

defined( '_JEXEC' ) or die;

use JLoader;
use Joomla\CMS\Factory;
use Joomla\CMS\Log\Log;
use Joomla\CMS\MVC\Controller\BaseController;

JLoader::registerNamespace( 'Webt', JPATH_LIBRARIES . '/Webt' );

use Webt\Model\ArticleModel;
use Webt\Model\SettingsModel;
use Webt\Services\TranslationManager;

/**
 * @package     Joomla.Administrator
 * @subpackage  com_webt
 *
 * @copyright
 * @license     GNU General Public License version 2 or later; see License.txt
 */

/**
 * Controller responsible for all things related to machine translation.
 */
class MachineTranslationController extends BaseController {
	/**
	 * The default view for the display method.
	 *
	 * @var string
	 */
	protected $default_view = 'MachineTranslation';

	/**
	 * Method that handles the request to translate articles.
	 * This method is invoked when the 'submit' task is called in the controller.
	 *
	 * @return void
	 */
	public function submit() {
		$this->checkToken();

		$input               = Factory::getApplication()->input;
		$targetLanguageArray = $input->get( 'cid', array() );
		if ( empty( $targetLanguageArray ) ) {
			Factory::getApplication()->enqueueMessage( 'Please select a language to translate!', 'warning' );
		} else {
			$settingsModel = new SettingsModel();
			$passCheck     = true;

			// Check if languages are content languages
			foreach ( $targetLanguageArray as $language ) {
				if ( ! in_array( $language, array_column( $settingsModel->availableLanguages, 'lang_code' ) ) ) {
					$passCheck = false;
				}
			}
			if ( $passCheck ) {
				$this->translateArticles( $targetLanguageArray );
			} else {
				Factory::getApplication()->enqueueMessage( 'Please select a language to translate!', 'warning' );
			}
		}
        $this->display();
	}

	/**
	 * Private method to handle the translation of articles to the target languages.
	 *
	 * @param array $targetLanguageArray An array of target language codes.
	 * @return void
	 */
	private function translateArticles( $targetLanguageArray ) {
		$stringLanguages = implode(', ', $targetLanguageArray);
		Log::add( "Starting pre-translation for languages $stringLanguages", Log::INFO, 'webt' );

		$settingsModel = new SettingsModel();
		$articleModel = new ArticleModel();

		$sourceLanguage      = $settingsModel->getDefaultLanguage();
		$sourceArticles = $articleModel->getArticles( $sourceLanguage );

		$translationManager = new TranslationManager();
		foreach ( $sourceArticles as $article ) {
			$translationManager->translateArticle( $article, $targetLanguageArray );
		}

		Factory::getApplication()->enqueueMessage( 'Translation completed!' );
	}
}
