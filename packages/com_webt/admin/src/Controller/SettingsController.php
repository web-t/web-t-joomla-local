<?php

namespace Component\Webt\Administrator\Controller;

defined( '_JEXEC' ) or die;

use JLoader;
use Joomla\CMS\Factory;
use Joomla\CMS\Log\Log;
use Joomla\CMS\MVC\Controller\BaseController;

JLoader::registerNamespace( 'Webt', JPATH_LIBRARIES . '/Webt' );

use Webt\Model\SettingsModel;
use Webt\Services\EtranslationUtils;
use Webt\Services\TranslationManager;

/**
 * @package     Joomla.Administrator
 * @subpackage  com_webt
 *
 * @copyright
 * @license     GNU General Public License version 2 or later; see License.txt
 */

/**
 * Controller responsible for all things related to settings
 */
class SettingsController extends BaseController {
	/**
	 * The default view for the display method.
	 *
	 * @var string
	 */
	protected $default_view = 'Settings';

	/**
	 * The submit method handles the submission of settings.
	 */
	public function submit() {
		$settingsModel = new SettingsModel();

		$input  = Factory::getApplication()->input;
		$engine = $input->get( 'mtEngine', 'etranslation' );

		$previousEngine = $settingsModel->getMtEngine();
		$settingsModel->setMtEngine( $engine );

		if ( $this->validate( $input, $engine ) ) {
			if ( 'etranslation' === $engine ) {
				$settingsModel->setEtranslationApplicationName( $input->get( 'ApplicationName', '', 'html' ) );
				$password = $input->get( 'Password', '', 'html' );
				if ( $password !== $settingsModel->getEtranslationPassword() ) {
					$settingsModel->setEtranslationPassword( EtranslationUtils::encrypt_password( $password ) );
				}
			} elseif ( 'customProvider' === $engine ) {
				$currentApiKey = $settingsModel->getApiKey();
				$currentApiUrl = $settingsModel->getUrl();
				$newApiKey     = $input->get( 'ApiKey', '', 'html' );
				$newApiUrl     = $input->get( 'MTUrl', '', 'html' );
				$settingsModel->setApiKey( $newApiKey );
				$settingsModel->setUrl( $newApiUrl );
				if ( $currentApiKey !== $newApiKey || $currentApiUrl != $newApiUrl ) {
					$settingsModel->setSupportedEngines( null );
					$settingsModel->setSelectedEngines( null );
				}
			}
			if ( $engine !== $previousEngine ) {
				$settingsModel->setSupportedEngines( null );
			}
			if ( $this->checkMtConnection() ) {
				Factory::getApplication()->enqueueMessage( 'MT provider connection successful!' );
			} else {
				Factory::getApplication()->enqueueMessage( 'There was a problem connecting to the MT provider! Please recheck your provider configuration or see logs for details.', 'error' );
			}
			Factory::getApplication()->enqueueMessage( 'Settings saved!' );
		}

		$this->display();
	}
	/**
	 * Private method to validate input based on the selected engine.
	 *
	 * @param object $input The input object.
	 * @param string $engine The selected machine translation engine.
	 * @return bool Returns true if the input is valid, otherwise false.
	 */
	private function validate( $input, $engine ) {
		if ( 'etranslation' === $engine ) {
			if ( ! $input->get( 'ApplicationName', '', 'html' ) ) {
				Factory::getApplication()->enqueueMessage( 'Please, enter your eTranslation API application name!', 'warning' );
				return false;
			}
			if ( ! $input->get( 'Password', '', 'html' ) ) {
				Factory::getApplication()->enqueueMessage( 'Please, enter your eTranslation API password!', 'warning' );
				return false;
			}
		} elseif ( 'customProvider' === $engine ) {
			$url = $input->get( 'MTUrl', '', 'html' );
			if ( ! $url ) {
				Factory::getApplication()->enqueueMessage( 'Please, enter your Translation provider API Base URL!', 'warning' );
				return false;
			}
			if ( ! filter_var( $url, FILTER_VALIDATE_URL ) ) {
				Factory::getApplication()->enqueueMessage( 'Please, enter a valid URL from your MT provider!', 'warning' );
				return false;
			}
			if ( ! $input->get( 'ApiKey', '', 'html' ) ) {
				Factory::getApplication()->enqueueMessage( 'Please enter a valid API key from your MT provider!', 'warning' );
				return false;
			}
		}
		return true;
	}

	/**
	 * Private method to check the connection to the MT provider.
	 */
	private function checkMtConnection() {
		$translationManager = new TranslationManager();
		return $translationManager->translationService->testConnection();
	}
}
