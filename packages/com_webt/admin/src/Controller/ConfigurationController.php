<?php

namespace Component\Webt\Administrator\Controller;

defined( '_JEXEC' ) or die;

use Joomla\CMS\MVC\Controller\BaseController;
use Joomla\CMS\Factory;
use Joomla\CMS\Log\Log;
use JLoader;

JLoader::registerNamespace( 'Webt', JPATH_LIBRARIES . '/Webt' );

use Webt\Model\SettingsModel;

/**
 * @package     Joomla.Administrator
 * @subpackage  com_webt
 *
 * @copyright
 * @license     GNU General Public License version 2 or later; see License.txt
 */

/**
 * Controller responsible for all things related to configuration
 */
class ConfigurationController extends BaseController {
	/**
	 * The default view for the display method.
	 *
	 * @var string
	 */
	protected $default_view = 'Configuration';

    /**
	 * The save method handles saving configuration settings.
	 */
    public function save() {
		$this->checkToken();

		$input             = Factory::getApplication()->input;
		$on                = 'on';
		$inheritProperties = $on === $input->get( 'inheritProperties' );
		$translateOnVisit  = $on === $input->get( 'translateOnVisit' );
		$showMtNotice      = $on === $input->get( 'showMtNotice' );

		$etranslationTimeout = $input->getInt( 'etranslationTimeout' );

        $settingsModel = new SettingsModel();
        $settingsModel->setInheritArticleProperties( $inheritProperties );
        $settingsModel->setTranslateOnVisit( $translateOnVisit );
        $settingsModel->setShowMtNotice( $showMtNotice );
        $settingsModel->setEtranslationTimeout( $etranslationTimeout );
		$settingsModel->setSelectedEngines( $input->getHtml( 'engines' ) );

        Factory::getApplication()->enqueueMessage( 'Configuration saved!' );
        
        $this->display();
    }
}
