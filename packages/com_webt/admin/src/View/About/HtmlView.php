<?php

namespace Component\Webt\Administrator\View\About;

defined( '_JEXEC' ) or die;

use Joomla\CMS\Toolbar\ToolbarHelper;
use Joomla\CMS\MVC\View\HtmlView as BaseHtmlView;

/**
 * Class declaration for the about view.
 *
 * @license     GNU General Public License version 2 or later, see License.txt
 */
class HtmlView extends BaseHtmlView {

	/**
     * The display method is responsible for rendering the view's output.
     * It can take an optional $tpl parameter to specify the template to use.
     *
     * @param string|null $tpl The name of the template to be used for rendering the view.
     */
    public function display( $tpl = null ) {
		$this->addToolbar();
		parent::display( $tpl );
	}

	/**
     * The addToolbar method is used to add a toolbar to the view.
     */
	protected function addToolbar(): void {
		ToolbarHelper::title( 'WEB-T: About' );
	}
}
