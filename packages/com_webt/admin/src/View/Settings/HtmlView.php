<?php

namespace Component\Webt\Administrator\View\Settings;

defined( '_JEXEC' ) or die;

use Joomla\CMS\MVC\View\HtmlView as BaseHtmlView;
use Joomla\CMS\Toolbar\ToolbarHelper;
use JLoader;

JLoader::registerNamespace( 'Webt', JPATH_LIBRARIES . '/Webt' );

use Webt\Model\SettingsModel;

define( '__ROOT__', ( dirname( __FILE__, 4 ) ) );

/**
 * @package     Joomla.Administrator
 * @subpackage  com_webt
 *
 * @copyright
 * @license     GNU General Public License version 2 or later; see License.txt
 */
/**
 * Class declaration for the settings view.
 */
class HtmlView extends BaseHtmlView {

	/**
	 * Api key to use to login MT system.
	 *
	 * @var string
	 */
	protected $apiKey;
	/**
	 * MT system url.
	 *
	 * @var string
	 */
	/**
	 * The form object for the newsfeed
	 *
	 * @var  \Joomla\CMS\Form\Form
	 */
	protected $form;
	/**
	 * MT url used for translation
	 *
	 * @var  string
	 */
	protected $MTUrl;
	/**
	 * MT engine for translation
	 *
	 * @var  string
	 */
    protected $mtEngine;
	/**
	 * Etranslation application name
	 *
	 * @var  string
	 */
    protected $applicationName;
	/**
	 * Etranslation password
	 *
	 * @var  string
	 */
    protected $password;
	/**
     * The display method is responsible for rendering the view's output.
     * It can take an optional $tpl parameter to specify the template to use.
     *
     * @param string|null $tpl The name of the template to be used for rendering the view.
     */
	function display( $tpl = null ) {
		$model        = new SettingsModel();
		$this->apiKey = $model->apiKey;
		$this->MTUrl  = $model->MTUrl;
        $this->mtEngine = $model->getMtEngine();
        $this->applicationName = $model->getEtranslationApplicationName();
        $this->password = $model->getEtranslationPassword();

		$this->addToolbar();

		parent::display( $tpl );
	}
	/**
     * The addToolbar method is used to add a toolbar to the view.
     */
	protected function addToolbar(): void {
		ToolbarHelper::title( 'WEB-T: Translation provider' );
		ToolbarHelper::save( 'Settings.submit', 'Save' );
	}
}
