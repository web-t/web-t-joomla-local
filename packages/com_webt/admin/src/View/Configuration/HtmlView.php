<?php

namespace Component\Webt\Administrator\View\Configuration;

defined( '_JEXEC' ) or die;

use Joomla\CMS\Factory;
use Joomla\CMS\Toolbar\ToolbarHelper;
use Joomla\CMS\MVC\View\HtmlView as BaseHtmlView;
use JLoader;

JLoader::registerNamespace( 'Webt', JPATH_LIBRARIES . '/Webt' );

use Webt\Model\SettingsModel;
use Webt\Services\TranslationManager;

/**
 * Class declaration for the configuration view.
 *
 * @license     GNU General Public License version 2 or later, see License.txt
 */
class HtmlView extends BaseHtmlView {

    /** @var bool $inheritArticleProperties */
    protected $inheritArticleProperties;

    /** @var bool $translateOnVisit */
    protected $translateOnVisit;

    /** @var bool $showMtNotice */
    protected $showMtNotice;

    /** @var int $etranslationTimeout */
    protected $etranslationTimeout;

    /** @var string $mtEngine */
    protected $mtEngine;

    /** @var object[] $availableLanguages */
    protected $availableLanguages;

    /** @var array $supportedEngines */
    protected $supportedEngines;

    /** @var array $selectedEngines */
    protected $selectedEngines;

    /** @var string $defaultLangcode */
    private $defaultLangcode;
	/**
     * The display method is responsible for rendering the view's output.
     * It can take an optional $tpl parameter to specify the template to use.
     *
     * @param string|null $tpl The name of the template to be used for rendering the view.
     */
	public function display( $tpl = null ) {
		$settingsModel                  = new SettingsModel();
		$this->inheritArticleProperties = $settingsModel->getInheritArticleProperties();
		$this->translateOnVisit         = $settingsModel->getTranslateOnVisit();
		$this->showMtNotice             = $settingsModel->getShowMtNotice();
		$this->etranslationTimeout      = $settingsModel->getEtranslationTimeout();
		$this->mtEngine                 = $settingsModel->getMtEngine();
		$this->defaultLangcode          = $settingsModel->getDefaultLanguage();
		$this->availableLanguages       = $settingsModel->getLanguages();
		$this->supportedEngines         = $this->getMtEnginesForTargetLanguages();
		$this->selectedEngines          = $settingsModel->getSelectedEngines();

		$this->addToolbar();
		parent::display( $tpl );
	}
	/**
     * The addToolbar method is used to add a toolbar to the view.
     */
	protected function addToolbar(): void {
		ToolbarHelper::title( 'WEB-T: Configuration' );
		ToolbarHelper::save( 'Configuration.save', 'Save' );
	}
	/**
     * The getMtEnginesForTargetLanguages method retrieves the machine translation engines supported for target languages.
     *
     * @return array An array containing supported engines for each target language.
     */
	private function getMtEnginesForTargetLanguages() {
		$translationManager    = new TranslationManager();
		$engineList            = $translationManager->translationService->getSupportedEngineList();
		$default_language      = explode( '-', $this->defaultLangcode )[0];
		$result                = array();
		$unsupported_languages = array();
		foreach( $this->availableLanguages as $language ) {
			$engines = array();
			$target_language  = explode( '-', $language->lang_code )[0];
			foreach ( $engineList as $engine ) {
				if ( $default_language === $engine->srcLang && $target_language === $engine->trgLang ) {
					$engines[] = $engine;
				}
			}
			$result[ $language->lang_code ] = $engines;
			if ( empty( $engines ) ) {
				$unsupported_languages[] = $language->title;
			}
		}
		if ( count( $unsupported_languages ) > 0 ) {
			$list_str = implode( ', ', $unsupported_languages );
			Factory::getApplication()->enqueueMessage( "Current MT provider does not support following languages: $list_str! If you wish to translate these languages, contact the MT provider support or choose a different MT provider.", 'warning' );
		}
		return $result;
	}
}
