<?php

namespace Component\Webt\Administrator\View\MachineTranslation;

defined( '_JEXEC' ) or die;

use Joomla\CMS\MVC\View\HtmlView as BaseHtmlView;
use Joomla\CMS\Toolbar\ToolbarHelper;
use JLoader;

JLoader::registerNamespace( 'Webt', JPATH_LIBRARIES . '/Webt' );

use Webt\Model\SettingsModel;
use Webt\Model\ArticleModel;

/**
 * Class declaration for the Machine Translation view.
 * @license     GNU General Public License version 2 or later, see License.txt
 */
class HtmlView extends BaseHtmlView {

	/**
	 * Available languages user can select to translate to.
	 *
	 * @var array
	 */
	protected $availableLanguages;
	 /**
     * The display method is responsible for rendering the view's output.
     * It can take an optional $tpl parameter to specify the template to use.
     *
     * @param string|null $tpl The name of the template to be used for rendering the view.
     */
	public function display( $tpl = null ) {
		$model                    = new SettingsModel();
		$this->availableLanguages = $model->availableLanguages;
		$this->addToolbar();
		parent::display( $tpl );
	}
	/**
     * The addToolbar method is used to add a toolbar to the view.
     */
	protected function addToolbar(): void {
		ToolbarHelper::title( 'WEB-T: Machine translation' );
		ToolbarHelper::publish( 'MachineTranslation.submit', 'Translate articles', true );
	}
	/**
     * The calculateTranslationProgress method calculates the translation progress for each target language.
     * It counts the number of translated articles for each target language and calculates the progress as a percentage.
     *
     * @return array An array containing the translation progress for each target language.
     */
	protected function calculateTranslationProgress() {
		$articleModel = new ArticleModel();
		$settingsModel = new SettingsModel();

		$sourceLanguage = $settingsModel->getDefaultLanguage();
		$targetLanguages = $settingsModel->getLanguages();

		$sourceArticles = $articleModel->getArticles( $sourceLanguage );
		$sourceCount = count( $sourceArticles );

		$percentages = array();
		foreach( $targetLanguages as $targetLanguage ) {
			$translation_count = 0;
			foreach( $sourceArticles as $article ) {
				if ( $articleModel->getArticleTranslation( $article->id, $targetLanguage->lang_code ) ) {
					$translation_count++;
				}
			}
			$percentages[ $targetLanguage->lang_code ] = $sourceCount > 0 ? round( 100 * $translation_count / $sourceCount, 2 ) : 0;
		}
		return $percentages;
	}
}
