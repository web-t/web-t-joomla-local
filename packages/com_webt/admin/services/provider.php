<?php

defined('_JEXEC') or die;

use Joomla\CMS\Dispatcher\ComponentDispatcherFactoryInterface;
use Joomla\CMS\Extension\ComponentInterface;
use Joomla\CMS\Extension\MVCComponent;
use Joomla\CMS\Extension\Service\Provider\ComponentDispatcherFactory;
use Joomla\CMS\Extension\Service\Provider\MVCFactory;
use Joomla\CMS\MVC\Factory\MVCFactoryInterface;
use Joomla\DI\Container;
use Joomla\DI\ServiceProviderInterface;

/**
 * Return a new anonymous class implementing the ServiceProviderInterface.
 *
 * @license     GNU General Public License version 2 or later, see License.txt
 */
return new class implements ServiceProviderInterface {
    public function register(Container $container): void {
        // Register the MVCFactory service for the "com_webt" component namespace.
        $container->registerServiceProvider(new MVCFactory('\\Component\\Webt'));

        // Register the ComponentDispatcherFactory service for the "com_webt" component namespace.
        $container->registerServiceProvider(new ComponentDispatcherFactory('\\Component\\Webt'));

        $container->set(
            ComponentInterface::class,
            function (Container $container) {
                $component = new MVCComponent($container->get(ComponentDispatcherFactoryInterface::class));
                $component->setMVCFactory($container->get(MVCFactoryInterface::class));
                return $component;
            }
        );
    }
};