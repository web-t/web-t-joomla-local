<?php

namespace Webt\Services;

defined( '_JEXEC' ) or die;

use Webt\Model\SettingsModel;
use Exception;
use Joomla\CMS\Factory;
use Joomla\CMS\Http\Http;
use Joomla\CMS\Log\Log;

/**
 * This is an abstract PHP class named `AbstractTranslationService`.
 * It serves as the base class for specific translation service implementations.
 *
 * @license     GNU General Public License version 2 or later, see License.txt
 */
abstract class AbstractTranslationService {

	/** @var $http_client Http client used to send requests */
	protected $http_client;
	/** @var $retry_on_error Check if retry on error */
	protected $retry_on_error = true;
	/** @var $max_chars_per_request Character limit per request */
	protected $max_chars_per_request = 60000;
	/** @var $selected_domains Selected domain name */
	protected $selected_domains;
	/**
	 * Constructor method that initializes the HTTP client and retrieves selected MT engines.
	 */
	public function __construct() {
		$http_client_factory     = new Http();
		$settingsModel           = new SettingsModel();
		$this->selected_domains  = $settingsModel->getSelectedEngines();
		$this->http_client       = $http_client_factory->setOption('timeout', 120);
	}
	/**
	 * Abstract method for sending a translation request to the specific MT engine.
	 * Subclasses must implement this method to handle translation requests.
	 *
	 * @param string $from The source language code.
	 * @param string $to The target language code.
	 * @param array $values An array of strings to be translated.
	 * @param bool $is_pretranslation Indicates if it is a pre-translation request.
	 * @param int|null $article_id The ID of the article (optional, used for logging).
	 * @return array An array of translated strings or an empty array on failure.
	 */
	abstract protected function send_translation_request( $from, $to, $values, $is_pretranslation = true, $article_id = null );
	/**
	 * Abstract method for sending a language direction request to the MT engine.
	 * Subclasses must implement this method to handle language direction requests.
	 *
	 * @return mixed The response from the MT engine or null on failure.
	 */
	abstract protected function send_language_direction_request();
	/**
	 * Translates an array of strings from the source language to the target language.
	 * It splits large requests into multiple smaller requests if necessary.
	 *
	 * @param string $from The source language code.
	 * @param string $to The target language code.
	 * @param array $values An array of strings to be translated.
	 * @param bool $is_pretranslation Indicates if it is a pre-translation request.
	 * @param int|null $article_id The ID of the article (optional, used for logging).
	 * @return array An array of translated strings or an empty array on failure.
	 */
	public function translate( $from, $to, $values, $is_pretranslation = true, $article_id = null ) {

		$current_char_len    = 0;
		$request_value_lists = array();
		$current_list        = array();

		// split into multiple arrays not exceeding max char limit.
		foreach ( $values as $value ) {
			$chars = strlen( $value );
			if ( $current_char_len + $chars > $this->max_chars_per_request ) {
				$request_value_lists[] = $current_list;
				$current_list          = array( $value );
				$current_char_len      = $chars;
			} else {
				$current_list[]    = $value;
				$current_char_len += $chars;
			}
		}
		if ( ! empty( $current_list ) ) {
			$request_value_lists[] = $current_list;
		}

		// send requests and merge results together.
		$full_result   = array();
		$request_count = count( $request_value_lists );

		for ( $i = 0; $i < $request_count; $i++ ) {

			$contains_only_empty_strings = empty(
				array_filter(
					$request_value_lists[ $i ],
					function ( $a ) {
						return ! empty( $a );
					}
				)
			);

			if ( $contains_only_empty_strings ) {
				$full_result = array_merge( $full_result, $request_value_lists[ $i ] );
			} else {
				Log::add("Sending $from->$to translation request " . $i + 1 . '/' . $request_count . ' (' . count( $request_value_lists[ $i ] ) . ' strings)', Log::DEBUG, 'webt' );
				$translations = $this->send_translation_request( $from, $to, $request_value_lists[ $i ], $is_pretranslation, $article_id );

				if ( ! $translations || empty( $translations ) ) {
					$retries_left = 2;
					while ( $retries_left > 0 && $this->retry_on_error && $is_pretranslation ) {
						Log::add("Translation request failed, retrying... (retries left: $retries_left)", Log::DEBUG, 'webt' );
						$translations = $this->send_translation_request( $from, $to, $request_value_lists[ $i ], $is_pretranslation, $article_id );
						$retries_left--;
					}

					// do not continue if one of requests fail.
					if ( ! $translations || empty( $translations ) ) {
						if ( $request_count > 1 ) {
							Log::add('One of translation requests failed', Log::ERROR, 'webt' );
						}
						return array();
					}
				}
				// merge with previous translations.
				$full_result = array_merge( $full_result, $translations );
			}
		}
		return $full_result;
	}
	/**
	 * Tests the connection to the MT engine by sending a language direction request.
	 *
	 * @return bool Returns true if the connection is successful, false otherwise.
	 */
	public function testConnection() {
		$response = $this->send_language_direction_request();
		if ( ! $response ) {
			return false;
		}
		$content_type = isset( $response->getHeaders()['content-type'] ) ? $response->getHeaders()['content-type'][0] : null;
		if ( $content_type && ! str_contains( $content_type, 'application/json' ) ) {
			Log::add( "Invalid content type '$content_type' in language direction response!", Log::ERROR, 'webt' );
			return false;
		}
		return 200 === $response->getStatusCode();
	}
	/**
	 * Retrieves the list of supported MT engines.
	 * If the list is not available, it sends a language direction request to fetch it.
	 *
	 * @return array An array of supported MT engines or an empty array on failure.
	 */
	public function getSupportedEngineList() {
		$settingsModel    = new SettingsModel();
		$supportedEngines = $settingsModel->getSupportedEngines();
		if ( ! $supportedEngines ) {
			try 
			{
				$response = $this->send_language_direction_request();
				if ( 200 === $response->getStatusCode() ) {
					$json_string      = $this->map_language_direction_response_body( $response->getBody() );
					$supportedEngines = json_decode( $json_string );
					$settingsModel->setSupportedEngines( $supportedEngines );
				}
			}
			catch (Exception)
			{
				$application = Factory::getApplication();
				if ( $application instanceof AdministratorApplication ) {
					$application->enqueueMessage( "MT provider data is not set. Please configure it in the WEB-T > Translation provider tab.", 'error' );
 				}
				return array();
			}
		}
		return $supportedEngines ? $supportedEngines->languageDirections : array();
	}
	/**
	 * Helper method to map the response body of a language direction request.
	 * Subclasses can override this method to handle specific mapping logic if needed.
	 *
	 * @param mixed $body The response body from the language direction request.
	 * @return string The mapped response body as a string.
	 */
	protected function map_language_direction_response_body( $body ) {
		return (string) $body;
	}
}
