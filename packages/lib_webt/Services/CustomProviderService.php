<?php

namespace Webt\Services;

defined( '_JEXEC' ) or die;

use Webt\Model\SettingsModel;
use Exception;
use Joomla\CMS\Log\Log;

/**
 * This is a PHP class named `CustomProviderService`.
 * It extends the `AbstractTranslationService` class and provides implementation
 * for the translation requests using a custom translation provider.
 *
 * @license     GNU General Public License version 2 or later, see License.txt
 */
class CustomProviderService extends AbstractTranslationService {

	/** @var $http_client Http client used to send requests */
	protected $http_client;
	/** @var $apiKey Custom provider api key */
	protected $apiKey;
	/** @var $url Custom provider url */
	protected $url;
	/**
	 * Constructor method that initializes the HTTP client, API key, and URL
	 * by fetching the configuration from the settings model and calling the parent constructor.
	 */
	public function __construct() {
		$settingsModel = new SettingsModel();
		$this->apiKey  = $settingsModel->getApiKey();
		$this->url     = $settingsModel->getUrl();
		parent::__construct();
	}
	/**
	 * Implementation of the `send_translation_request` method.
	 * Sends a translation request to the custom translation provider.
	 *
	 * @param string $from The source language code.
	 * @param string $to The target language code.
	 * @param array $values An array of strings to be translated.
	 * @param bool $is_pretranslation Indicates if it is a pre-translation request.
	 * @param int|null $article_id The ID of the article (optional, used for logging).
	 * @return array An array of translated strings or an empty array on failure.
	 * @throws Exception If the translation provider is not configured.
	 */
	protected function send_translation_request( $from, $to, $values, $is_pretranslation = true, $article_id = null ) {
		$data          = (object) array();
		$data->srcLang = explode( '-', $from )[0];    // Get the language codes
		$data->trgLang = explode( '-', $to )[0];
		$data->text    = $values;

        if ( isset( $this->selected_domains->$to ) && $this->selected_domains->$to ) {
            $data->domain = $this->selected_domains->$to;
        }

		if ( ! $this->url || ! $this->apiKey ) {
			throw new Exception( 'Translation provider not configured!' );
		}

		$headers = array(
			'Content-Type' => 'application/json',
			'Accept'       => 'text/plain',
			'X-API-KEY'    => $this->apiKey,
		);
		$body    = json_encode( $data, JSON_UNESCAPED_SLASHES );

		Log::add( "Sending $from-$to translation request.", Log::DEBUG, 'webt' );
        $response = $this->http_client->post( $this->url . '/translate/text', $body, $headers );
        $json     = json_decode( $response->getBody() );
		Log::add( "Received $from-$to response.", Log::DEBUG, 'webt' );

        if ( $response->getStatusCode() == 404 ) {
			Log::add( "Translation endpoint not found! Please check if the custom provider settings are properly configured at WEB-T > Translation provider tab and that this custom provider supports $from-$to translation.", Log::ERROR, 'webt' );
            return array();
        }
        if ( $response->getStatusCode() != 200 ) {
			Log::add( "Invalid response status code from translation provider: " . $response->getStatusCode(), Log::ERROR, 'webt' );
            return array();
        }

        $translations = array_map(
            function( $item ) {
                return $item->translation;
            },
            $json->translations
        );
        return $translations;
	}
	/**
	 * Implementation of the `send_language_direction_request` method.
	 * Sends a language direction request to the custom translation provider.
	 *
	 * @return mixed The response from the custom translation provider or null on failure.
	 * @throws Exception If the translation provider is not configured.
	 */
	protected function send_language_direction_request() {
		if ( ! $this->url || ! $this->apiKey ) {
			throw new Exception( 'Translation provider not configured!' );
		}

		$headers = array(
			'accept'    => 'application/json',
			'X-API-KEY' => $this->apiKey,
		);

		try {
			return $this->http_client->get( $this->url . '/translate/language-directions', $headers );
		} catch ( Exception $e ) {
			$message = (string) $e->getMessage();
			Log::add( "Error on language direction request: $message", Log::ERROR, 'webt' );
			return null;
		}
	}
}
