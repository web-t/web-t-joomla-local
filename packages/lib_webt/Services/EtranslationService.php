<?php

namespace Webt\Services;

defined( '_JEXEC' ) or die;

use Webt\Model\EtranslationResponseModel;
use Webt\Model\SettingsModel;
use Webt\Services\EtranslationUtils;
use Joomla\CMS\Log\Log;
use Joomla\CMS\Uri\Uri;
use Exception;

/**
 * This is a PHP class named `EtranslationService`.
 * It extends the `AbstractTranslationService` class and provides implementation
 * for translation requests using the eTranslation service provided by the European Commission.
 *
 * @license     GNU General Public License version 2 or later, see License.txt
 */
class EtranslationService extends AbstractTranslationService {

	private static $error_map = array(
		-20000 => 'Source language not specified',
		-20001 => 'Invalid source language',
		-20002 => 'Target language(s) not specified',
		-20003 => 'Invalid target language(s)',
		-20004 => 'DEPRECATED',
		-20005 => 'Caller information not specified',
		-20006 => 'Missing application name',
		-20007 => 'Application not authorized to access the service',
		-20008 => 'Bad format for ftp address',
		-20009 => 'Bad format for sftp address',
		-20010 => 'Bad format for http address',
		-20011 => 'Bad format for email address',
		-20012 => 'Translation request must be text type, document path type or document base64 type and not several at a time',
		-20013 => 'Language pair not supported by the domain',
		-20014 => 'Username parameter not specified',
		-20015 => 'Extension invalid compared to the MIME type',
		-20016 => 'DEPRECATED',
		-20017 => 'Username parameter too long',
		-20018 => 'Invalid output format',
		-20019 => 'Institution parameter too long',
		-20020 => 'Department number too long',
		-20021 => 'Text to translate too long',
		-20022 => 'Too many FTP destinations',
		-20023 => 'Too many SFTP destinations',
		-20024 => 'Too many HTTP destinations',
		-20025 => 'Missing destination',
		-20026 => 'Bad requester callback protocol',
		-20027 => 'Bad error callback protocol',
		-20028 => 'Concurrency quota exceeded',
		-20029 => 'Document format not supported',
		-20030 => 'Text to translate is empty',
		-20031 => 'Missing text or document to translate',
		-20032 => 'Email address too long',
		-20033 => 'Cannot read stream',
		-20034 => 'Output format not supported',
		-20035 => 'Email destination tag is missing or empty',
		-20036 => 'HTTP destination tag is missing or empty',
		-20037 => 'FTP destination tag is missing or empty',
		-20038 => 'SFTP destination tag is missing or empty',
		-20039 => 'Document to translate tag is missing or empty',
		-20040 => 'Format tag is missing or empty',
		-20041 => 'The content is missing or empty',
		-20042 => 'Source language defined in TMX file differs from request',
		-20043 => 'Source language defined in XLIFF file differs from request',
		-20044 => 'Output format is not available when quality estimate is requested. It should be blank or \'xslx\'',
		-20045 => 'Quality estimate is not available for text snippet',
		-20046 => 'Document too big (>20Mb)',
		-20047 => 'Quality estimation not available',
		-40010 => 'Too many segments to translate',
		-80004 => 'Cannot store notification file at specified FTP address',
		-80005 => 'Cannot store notification file at specified SFTP address',
		-80006 => 'Cannot store translated file at specified FTP address',
		-80007 => 'Cannot store translated file at specified SFTP address',
		-90000 => 'Cannot connect to FTP',
		-90001 => 'Cannot retrieve file at specified FTP address',
		-90002 => 'File not found at specified address on FTP',
		-90007 => 'Malformed FTP address',
		-90012 => 'Cannot retrieve file content on SFTP',
		-90013 => 'Cannot connect to SFTP',
		-90014 => 'Cannot store file at specified FTP address',
		-90015 => 'Cannot retrieve file content on SFTP',
		-90016 => 'Cannot retrieve file at specified SFTP address',
	);

	/** @var string $api_url Default api url for etranslation */
	private static $api_url = 'https://webgate.ec.europa.eu/etranslation/si';
	// do not retry if response has not been received within specified $etranslation_timeout on entity insert/update.
	/** @var bool $retry_on_error Check if retry on error */
	protected $retry_on_error = false;
	/** @var int $request_timeout Request timeout in seconds */
	private $request_timeout = 600;
	/** @var string $application_name Etranslation application name */
	private $application_name;
	/** @var string $password Etranslation password */
	private $password;
	/** @var $etranslation_timeout Etranslation timeout*/
	private $etranslation_timeout;
	/** @var int $max_chars_per_request Maximum character per request */
	protected $max_chars_per_request   = 60000;
	/** @var int $default_chars_per_request Default character per request */
	private $default_chars_per_request = 60000;
	/**
	 * Constructor method that initializes the eTranslation credentials and timeouts
	 * by fetching the configuration from the settings model and calling the parent constructor.
	 */
	public function __construct() {
		$settingsModel               = new SettingsModel();
		$this->application_name      = $settingsModel->getEtranslationApplicationName();
		$this->password              = EtranslationUtils::decrypt_password( $settingsModel->getEtranslationPassword() );
		$this->etranslation_timeout  = $settingsModel->getEtranslationTimeout();
		$this->max_chars_per_request = $this->default_chars_per_request;
		parent::__construct();
	}
	/**
	 * Sends a language direction request to the eTranslation service to get the supported language directions.
	 *
	 * @return mixed The response from the eTranslation service or null on failure.
	 * @throws Exception If eTranslation credentials are not configured or the request fails.
	 */
	public function send_translation_request( $from, $to, $values, $is_pretranslation = true, $article_id = null ) {

		if ( ! $this->application_name || ! $this->password ) {
			throw new Exception( 'eTranslation credentials not configured!' );
		}

		$lang_from = explode( '-', $from )[0];
		$lang_to   = explode( '-', $to )[0];

		$domain = ( isset( $this->selected_domains->$to ) && $this->selected_domains->$to ) ? $this->selected_domains->$to : null;

		// prepare translation request.
		$id      = uniqid();
		$content = EtranslationUtils::encode_request_body( $values );

		if ( strlen( $content ) === 0 ) {
			return array();
		}

		$post   = $this->get_post_body( $id, $lang_from, $lang_to, $content, $domain, $article_id );
		$client = $this->get_curl_client( $post, '/translate' );

		Log::add( "Sending an eTranslation request [ID=$id]", Log::DEBUG, 'webt' );
		// send translation request.
		$response    = curl_exec( $client );
		$http_status = curl_getinfo( $client, CURLINFO_RESPONSE_CODE );
		curl_close( $client );

		// check request response.
		$body       = json_decode( $response );
		$request_id = is_numeric( $body ) ? (int) $body : null;
		if ( 200 !== $http_status || $request_id < 0 ) {
			$message = self::$error_map[ $request_id ] ?? $body;
			$err     = curl_error( $client );
			Log::add( "Invalid request response from eTranslation: $response [status: $http_status, message: $message, error: $err]", Log::ERROR, 'webt' );
			return array();
		}

		Log::add( "eTranslation request successful ($request_id) [ID=$id]", Log::DEBUG, 'webt' );

		// wait for translation callback.
		$response = $this->await_etranslation_response( $id, $article_id, $to, $is_pretranslation );
		if ( $response ) {
			return EtranslationUtils::decode_response( $response );
		} else {
			return array();
		}
	}
    /**
     * Retrieves the cURL client to make HTTP requests to the eTranslation service.
     *
     * @param string $body The request body to be sent.
     * @param string $url_part The URL part for the request endpoint.
     * @return resource|false Returns a cURL handle on success or false on failure.
     */
	private function get_curl_client( $body, $url_part ) {
		$application_name = $this->application_name;
		$password         = $this->password;

		$client = curl_init( self::$api_url . $url_part );
		curl_setopt( $client, CURLOPT_POST, 1 );
		curl_setopt( $client, CURLOPT_POSTFIELDS, $body );
		curl_setopt( $client, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt( $client, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST );
		curl_setopt( $client, CURLOPT_USERPWD, $application_name . ':' . $password );
		curl_setopt( $client, CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $client, CURLOPT_FOLLOWLOCATION, true );
		curl_setopt( $client, CURLOPT_TIMEOUT, $this->request_timeout );
		curl_setopt(
			$client,
			CURLOPT_HTTPHEADER,
			array(
				'Content-Type: application/json',
				'Content-Length: ' . strlen( $body ),
			)
		);
		return $client;
	}
    /**
     * Prepares the POST body for the eTranslation translation request.
     *
     * @param string $id The unique ID for the translation request.
     * @param string $lang_from The source language code.
     * @param string $lang_to The target language code.
     * @param string $translatable_string The content to be translated.
     * @param string|null $domain The translation domain (optional, if provided).
     * @param int|null $article_id The ID of the article (optional, if provided).
     * @return string JSON-encoded request body for the eTranslation service.
     */
	private function get_post_body( $id, $lang_from, $lang_to, $translatable_string, $domain, $article_id = null ) {
		$base_url     = str_replace( '/administrator/', '/', Uri::base() );
		$endpoint_url = $base_url . "api/index.php/v1/webtetranslation/$id";
		$document     = array(
			'content'  => $translatable_string,
			'format'   => 'html',
			'filename' => 'translateMe',
		);

		$translation_request_body = array(
			'documentToTranslateBase64' => $document,
			'sourceLanguage'            => strtoupper( $lang_from ),
			'targetLanguages'           => array(
				strtoupper( $lang_to ),
			),
			'errorCallback'             => $endpoint_url,
			'callerInformation'         => array(
				'application' => $this->application_name,
			),
			'destinations'              => array(
				'httpDestinations' => array(
					$endpoint_url,
				),
			),
		);
		if ( $domain ) {
			$translation_request_body['domain'] = $domain;
		}
		if ( $article_id ) {
			$translation_request_body['externalReference'] = $article_id;
		}
		return json_encode( $translation_request_body );
	}
    /**
     * Awaits the eTranslation response for a given request ID and article ID (if provided).
     *
     * @param string $request_id The unique ID of the translation request.
     * @param int|null $article_id The ID of the article (optional, used for logging).
     * @param string $target_langcode The target language code.
     * @param bool $is_pretranslation Indicates if it is a pre-translation request.
     * @return string|null The translated response or null on failure or timeout.
     */
	private function await_etranslation_response( $request_id, $article_id, $target_langcode, $is_pretranslation ) {
		$model = new EtranslationResponseModel();
		$model->insertTranslationEntry( $request_id, $article_id, $target_langcode );

		$response          = null;
		$start_time        = microtime( true );
		$check_interval_ms = 250;

		$timeout = $is_pretranslation ? $this->request_timeout : $this->etranslation_timeout;

		while ( ( ! $response || EtranslationUtils::$initial_value === $response ) && ( $timeout <= 0 || microtime( true ) - $start_time < $timeout ) ) {
			$response = $model->getTranslationEntry( $request_id )->response;
			usleep( $check_interval_ms * 1000 );
		}
		if ( $response && EtranslationUtils::$initial_value !== $response ) {
			$model->deleteTranslationEntry( $request_id );
			if ( EtranslationUtils::$error_value === $response ) {
				return null;
			}
			return $response;
		} else {
			Log::add( "eTranslation response timeout ($request_id, $timeout s)", Log::ERROR, 'eTranslation' );
			$model->updateTranslationEntry( $request_id, EtranslationUtils::$timeout_value );
			return null;
		}
	}
    /**
     * Maps the language direction request response to Generic API (Custom provider) JSON response.
     *
     * @param string $body Response body.
     * @return string JSON string representing the mapped response.
     */
	protected function send_language_direction_request() {
		$url = self::$api_url . '/get-domains';
		try {
			$auth_value = $this->get_digest_auth_header_value( $url, 'GET' );
			if ( $auth_value ) {
				$headers = array(
					'Authorization' => $auth_value,
				);
				return $this->http_client->get( $url, $headers );
			} else {
				Log::add( 'Could not get authorization header from eTranslation', Log::ERROR, 'webt' );
				return null;
			}
		} catch ( Exception $e ) {
			$message = (string) $e->getMessage();
			Log::add( "Error on language direction request: $message", Log::ERROR, 'webt' );
			return null;
		}
	}
	/**
     * Generates the Digest authentication header value for eTranslation API requests.
     *
     * @param string $request_url The URL for the API request.
     * @param string $method The HTTP method for the request.
     * @return string|false The Digest authentication header value or false on failure.
     */
	private function get_digest_auth_header_value( $request_url, $method ) {
		$response = $this->http_client->post( $request_url, null );
		$header   = $response->getHeader( 'WWW-Authenticate' ) ? $response->getHeader( 'WWW-Authenticate' )[0] : null;

		$retries = 2;
		while ( empty( $header ) && $retries > 0 ) {
			// request failed, retry.
			$response = $this->http_client->post( $request_url, null );
			$header   = $response->getHeader( 'WWW-Authenticate' ) ? $response->getHeader( 'WWW-Authenticate' )[0] : null;
			$retries--;
		}
		if ( empty( $header ) ) {
			return false;
		}

		/*
		* Parses the 'www-authenticate' header for nonce, realm, and other values.
		*/
		preg_match_all( '#(([\w]+)=["]?([^\s"]+))#', $header, $matches );
		$server_bits = array();
		foreach ( $matches[2] as $i => $key ) {
			$server_bits[ $key ] = $matches[3][ $i ];
		}

		$server_bits['realm'] = $server_bits['realm'] . ' Realm via Digest Authentication';
		$nc                   = '00000001';
		$path                 = parse_url( $request_url, PHP_URL_PATH );
		$client_nonce         = uniqid();
		$ha1                  = md5( $this->application_name . ':' . $server_bits['realm'] . ':' . $this->password );
		$ha2                  = md5( $method . ':' . $path );
		// The order of this array matters because it affects the resulting hashed val
		$response_bits = array(
			$ha1,
			$server_bits['nonce'],
			$nc,
			$client_nonce,
			$server_bits['qop'],
			$ha2,
		);

		$digest_header_values = array(
			'username'  => '"' . $this->application_name . '"',
			'realm'     => '"' . $server_bits['realm'] . '"',
			'nonce'     => '"' . $server_bits['nonce'] . '"',
			'uri'       => '"' . $path . '"',
			'algorithm' => '"MD5"',
			'qop'       => $server_bits['qop'],
			'nc'        => $nc,
			'cnonce'    => '"' . $client_nonce . '"',
			'response'  => '"' . md5( implode( ':', $response_bits ) ) . '"',
		);
		$digest_header        = 'Digest ';
		foreach ( $digest_header_values as $key => $value ) {
			$digest_header .= $key . '=' . $value . ', ';
		}
		$digest_header = rtrim( $digest_header, ', ' );
		return $digest_header;
	}

	/**
	 * Map language direction request response to Generic API (Custom provider) JSON response.
	 *
	 * @param string $body Response body.
	 * @return string
	 */
	protected function map_language_direction_response_body( $body ) {
		$systems = array();
		foreach ( json_decode( $body ) as $domain => $value ) {
			$lang_pairs = $value->languagePairs;
			foreach ( $lang_pairs as $pair ) {
				$lower = strtolower( $pair );
				$langs = explode( '-', $lower );

				$system          = new \stdClass();
				$system->srcLang = $langs[0];
				$system->trgLang = $langs[1];
				$system->domain  = $domain;
				$system->name    = $value->name;
				$systems[]       = $system;
			}
		}
		$response                     = new \stdClass();
		$response->languageDirections = $systems;
		return json_encode( $response );
	}
}
