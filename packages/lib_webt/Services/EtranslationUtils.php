<?php

namespace Webt\Services;

defined( '_JEXEC' ) or die;

use Joomla\CMS\Factory;

/**
 * This is a PHP utility class named `EtranslationUtils`.
 * It provides static utility functions related to encryption, decryption,
 * and encoding of data used in the eTranslation service.
 *
 * @license     GNU General Public License version 2 or later, see License.txt
 */
class EtranslationUtils {

	//Status variables
	public static $initial_value = '_translating';
	public static $timeout_value = '_timeout';
	public static $error_value   = '_error';

	//Replacing variables so that html values do not get translated
	private static $delimiter = '<param name="webt-delimiter" />';
	private static $prefix    = '<html>';
	private static $suffix    = '</html>';
    /**
     * Encrypts a plaintext password using AES-256-CBC encryption.
     *
     * @param string $plaintext The plaintext password to be encrypted.
     * @return string The base64-encoded ciphertext containing the encrypted password.
     */
	public static function encrypt_password( $plaintext ) {
		$config   = Factory::getConfig();
		$sitename = $config->get( 'sitename' );
		$method   = 'AES-256-CBC';
		$key      = hash( 'sha256', $sitename, true );
		$iv       = openssl_random_pseudo_bytes( 16 );

		$ciphertext = openssl_encrypt( $plaintext, $method, $key, OPENSSL_RAW_DATA, $iv );
		$hash       = hash_hmac( 'sha256', $ciphertext . $iv, $key, true );

		return base64_encode( $iv . $hash . $ciphertext );
	}
    /**
     * Decrypts a base64-encoded ciphertext to retrieve the original plaintext password.
     *
     * @param string $base64cipher The base64-encoded ciphertext containing the encrypted password.
     * @return string|null The decrypted plaintext password, or null if decryption fails.
     */
	public static function decrypt_password( $base64cipher ) {
		$config           = Factory::getConfig();
		$sitename         = $config->get( 'sitename' );
		$method           = 'AES-256-CBC';
		$ivHashCiphertext = base64_decode( $base64cipher );
		$iv               = substr( $ivHashCiphertext, 0, 16 );
		$hash             = substr( $ivHashCiphertext, 16, 32 );
		$ciphertext       = substr( $ivHashCiphertext, 48 );
		$key              = hash( 'sha256', $sitename, true );

		if ( ! hash_equals( hash_hmac( 'sha256', $ciphertext . $iv, $key, true ), $hash ) ) {
			return null;
		}

		return openssl_decrypt( $ciphertext, $method, $key, OPENSSL_RAW_DATA, $iv );
	}
    /**
     * Encodes an array of values into an HTML string and then base64-encodes it.
     *
     * @param array $values An array of values to be encoded.
     * @return string The base64-encoded HTML string containing the encoded values.
     */
	public static function encode_request_body( $values ) {
		$str  = implode( self::$delimiter, $values );
		$html = self::$prefix . $str . self::$suffix;
		return base64_encode( $html );
	}
    /**
     * Decodes a base64-encoded HTML string to retrieve an array of values.
     *
     * @param string $base64html The base64-encoded HTML string containing the encoded values.
     * @return array An array of decoded values extracted from the HTML string.
     */
	public static function decode_response( $base64html ) {
		$html = base64_decode( $base64html );
		$str  = substr( $html, strlen( self::$prefix ), strlen( $html ) - strlen( self::$prefix ) - strlen( self::$suffix ) );
		return explode( self::$delimiter, $str );
	}
}
