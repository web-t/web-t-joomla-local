<?php

namespace Webt\Services;

defined( '_JEXEC' ) or die;

use Joomla\CMS\Factory;
use Joomla\CMS\Log\Log;
use Joomla\CMS\Application\AdministratorApplication;
use Joomla\Component\Content\Site\Helper\AssociationHelper;
use Webt\Model\ArticleModel;
use Webt\Model\SettingsModel;
use Webt\Model\AssociationModel;
use Exception;

/**
 * This is a PHP class named `TranslationManager`, responsible for managing article translations using translation services.
 *
 * @license     GNU General Public License version 2 or later, see License.txt
 */
class TranslationManager {

	/**
	 * @var $translationService Chosen translation service that will be used for translation
	 */
	public $translationService;
    /**
     * Constructor for the `TranslationManager` class.
     * Initializes the translation service based on the configured machine translation engine.
     */
	public function __construct() {
		$settingsModel = new SettingsModel();
		if ( 'customProvider' === $settingsModel->getMtEngine() ) {
			$this->translationService = new CustomProviderService();
		} else {
			$this->translationService = new EtranslationService();
		}
	}
    /**
     * Translates an article into multiple target languages.
     *
     * @param object $article The source article object to be translated.
     * @param array $targetLanguages An array of target language codes for translation.
     * @param bool $force_retranslate Flag to force retranslation even if the translation already exists.
     * @param bool $createAssociations Flag to create article associations between the source and translated articles.
     * @param bool $is_pretranslation Flag to indicate whether the translation is a pre-translation or not.
     * @return array An associative array containing the association keys and translated article IDs.
     */
	public function translateArticle( $article, $targetLanguages, $force_retranslate = false, $createAssociations = true, $is_pretranslation = true ) {
		set_time_limit(180);
		$application = Factory::getApplication();

		$associationModel = new AssociationModel();
		$articleModel     = new ArticleModel();
		$sourceLanguage   = $article->language;

		$assocation    = $associationModel->getArticleAssociation( $article->id );
		$assocationKey = $assocation ? $assocation->key : $this->generateAssociationKey();

		if ( $createAssociations && ! $assocation ) {
			$associationModel->createArticleAssociation( $article->id, $assocationKey );
		}

		$supportedLanguages = $this->filterSupportedLanguages( $targetLanguages );
		if ( count( $supportedLanguages ) < count( $targetLanguages ) && $application instanceof AdministratorApplication ) {
			$list_str = implode( ', ', array_diff( $targetLanguages, $supportedLanguages ) );
			$application->enqueueMessage( "Could not translate to languages not supported by MT provider: $list_str! If you wish to translate these languages, contact the MT provider support or choose a different MT provider.", 'error' );
		}

		$result = array();

		Log::add( "Starting article $article->id translation...", Log::DEBUG, 'webt' );
		foreach ( $supportedLanguages as $targetLanguage ) {
			$translatableStrings = array( $article->title, $article->introtext, $article->fulltext, $article->metakey, $article->metadesc );
			try {
				$existingTranslation = $articleModel->getArticleTranslation( $article->id, $targetLanguage );
                if ( $existingTranslation && ! $force_retranslate ) {
                    // skip translation, if it already exists.
                    continue;
                }
				$translations = $this->translationService->translate( $sourceLanguage, $targetLanguage, $translatableStrings, $is_pretranslation, $article->id );
				if ( $translations && ! empty( $translations )  ) {
					$id = $this->upsertTranslatedArticle( $article, $targetLanguage, $translations, $existingTranslation );
					if ( empty( $result ) ) {
						$result = array( $assocationKey => array( $article->id ) );
					}
					$result[ $assocationKey ][] = $id;

					if ( $createAssociations && ! $existingTranslation ) {
						$associationModel->createArticleAssociation( $id, $assocationKey );
					}
				} else {
					throw new Exception( 'Translation was not received!' );
				}
			} catch ( \Exception $e ) {
				$message     = (string) $e->getMessage();
				$log_message = "Could not translate article ($sourceLanguage -> $targetLanguage). Article ID: " . $article->id . ", error: '$message'";
				Log::add( $log_message, Log::ERROR, 'webt' );

				// Add error notification if translating from backend.
				if ( $application instanceof AdministratorApplication ) {
					$application->enqueueMessage( $log_message, 'error' );
				}
				continue;
			}
		}
		Log::add( "Finished article $article->id translation.", Log::DEBUG, 'webt' );

		return $result;
	}
    /**
     * Translates an article based on provided translations.
     *
     * @param int $sourceArticleId The ID of the source article.
     * @param string $targetLanguage The target language code for translation.
     * @param array $translations An array containing translations for the article's fields.
     * @return void
     */
	public function translateArticleFromTextTranslation( $sourceArticleId, $targetLanguage, $translations ) {
		$articleModel     = new ArticleModel();
		$associationModel = new AssociationModel();

		$sourceArticle = $articleModel->getArticleById( $sourceArticleId );
		if ( $sourceArticle && $translations && ! empty( $translations ) ) {
			$assocation    = $associationModel->getArticleAssociation( $sourceArticleId);
			$assocationKey = $assocation ? $assocation->key : $this->generateAssociationKey();

			if ( ! $assocation->key ) {
				$associationModel->createArticleAssociation( $sourceArticleId, $assocationKey );
			}

			$existingTranslation = $articleModel->getArticleTranslation( $sourceArticleId, $targetLanguage );
			$id = $this->upsertTranslatedArticle( $sourceArticle, $targetLanguage, $translations, $existingTranslation );
			if ( ! $existingTranslation ) {
				$associationModel->createArticleAssociation( $id, $assocationKey );
			}
		}
	}

    /**
     * Inserts or updates the translated article based on the translations received.
     *
     * @param object $sourceArticle The source article object.
     * @param string $targetLanguage The target language code for translation.
     * @param array $translations An array containing translations for the article's fields.
     * @param object|null $existingTranslation The existing translated article object, if available.
     * @return int The ID of the inserted or updated translated article.
     */
	private function upsertTranslatedArticle( $sourceArticle, $targetLanguage, $translations, $existingTranslation ) {
		$articleModel = new ArticleModel();
		// Replace language code in title.
		$translations[0] = str_replace( $sourceArticle->language, $targetLanguage, $translations[0] );

		// Create/update translation article.
		if ( $existingTranslation ) {
			$articleModel->updateArticle( $existingTranslation->id, $translations, $sourceArticle );
			return $existingTranslation->id;
		} else {
			$id = $articleModel->insertArticle( $this->getCategoryIdForLangcode( $sourceArticle, $targetLanguage ), $targetLanguage, $translations, $sourceArticle );
			return $id;
		}
	}
    /**
     * Generates an association key for article associations.
     *
     * @return string A randomly generated association key.
     */
	private function generateAssociationKey() {
		$bytes = random_bytes( 32 );
		return substr( bin2hex( $bytes ), 0, 32 );
	}
    /**
     * Filters the supported target languages based on the available translation engines.
     *
     * @param array $targetLangcodes An array of target language codes for translation.
     * @return array An array of supported target language codes.
     */
	private function filterSupportedLanguages( $targetLangcodes ) {
		$engines            = $this->translationService->getSupportedEngineList();
		$settingsModel      = new SettingsModel();
		$defaultLangcode    = $settingsModel->getDefaultLanguage();
		$defaultLanguage    = explode( '-', $defaultLangcode )[0];
		$supportedLanguages = array();

		foreach ( $targetLangcodes as $targetLangcode ) {
			$targetLanguage = explode( '-', $targetLangcode )[0];
			foreach ( $engines as $engine ) {
				if ( $defaultLanguage === $engine->srcLang && $targetLanguage === $engine->trgLang ) {
					$supportedLanguages[] = $targetLangcode;
					break;
				}
			}
		}
		return array_unique( $supportedLanguages );
	}
    /**
     * Gets the category ID for the target language based on category associations.
     *
     * @param object $sourceArticle The source article object.
     * @param string $targetLanguage The target language code for translation.
     * @return int The category ID for the target language, or the source article's category ID if not found.
     */
	private function getCategoryIdForLangcode( $sourceArticle, $targetLanguage ) {
		$category_associations = AssociationHelper::getAssociations( $sourceArticle->catid, 'category' );
		if ( isset( $category_associations[ $targetLanguage ] ) ) {
			$params = explode( '&', $category_associations[ $targetLanguage ] );
			foreach( $params as $param ) {
				if ( str_starts_with( $param, 'id=' ) ) {
					return explode( '=', $param )[1];
				}
			}
		}
		return $sourceArticle->catid;
	}
}
