<?php

namespace Webt\Model;

defined( '_JEXEC' ) or die;

use Webt\Services\EtranslationUtils;
use Joomla\CMS\MVC\Model\BaseDatabaseModel;
use Joomla\CMS\Log\Log;

/**
 * The EtranslationResponseModel class is responsible for handling Etranslation response operations.
 *
 * @license     GNU General Public License version 2 or later, see License.txt
 */
class EtranslationResponseModel extends BaseDatabaseModel {

	/**
	 * @var $table Temporary table name
	 */
	private $table = '#__webttranslator_etranslation_temp';
	/**
     * Retrieves a translation entry from the temporary database table based on the provided request ID.
     *
     * @param string $request_id The request ID to retrieve the translation entry for.
     * @return object|null The translation entry object or null if not found.
     */
	public function getTranslationEntry( $request_id ) {
		$query = $this->_db->getQuery( true );

		// Select the required fields from the table.
		$query
			->select( '*' )
			->from( $this->_db->quoteName( $this->table ) )
			->where( $this->getRequestIdCondition( $request_id ) );

		$this->_db->setQuery( $query );
		return $this->_db->loadObject();
	}
	/**
     * Retrieves a translation entry from the temporary database table based on the provided source article ID and language code.
     *
     * @param int $source_article_id The ID of the source article to retrieve the translation entry for.
     * @param string $langcode The language code of the target language to retrieve the translation entry for.
     * @return object|null The translation entry object or null if not found.
     */
    public function getTranslationEntryByArticleIdAndLangcode( $source_article_id, $langcode ) {
		$query = $this->_db->getQuery( true );

        $conditions = array(
			$this->_db->quoteName( 'source_article' ) . ' = ' . $source_article_id,
			$this->_db->quoteName( 'target_langcode' ) . ' = ' . $this->_db->quote( $langcode ),
		);

		// Select the required fields from the table.
		$query
			->select( '*' )
			->from( $this->_db->quoteName( $this->table ) )
			->where( $conditions, 'AND' );

		$this->_db->setQuery( $query );
		return $this->_db->loadObject();
	}
	/**
     * Inserts a new translation entry into the temporary database table.
     *
     * @param string $request_id The request ID associated with the translation entry.
     * @param int $source_article_id The ID of the source article associated with the translation entry.
     * @param string $target_langcode The language code of the target language associated with the translation entry.
     * @return void
     */
	public function insertTranslationEntry( $request_id, $source_article_id, $target_langcode ) {
		Log::add( "Inserting temporary translation entry $request_id...", Log::DEBUG, 'webt' );

		$obj                  = new \stdClass();
		$obj->request_id      = $request_id;
		$obj->response        = EtranslationUtils::$initial_value;
		$obj->source_article  = $source_article_id;
		$obj->target_langcode = $target_langcode;

		$this->_db->insertObject( $this->table, $obj );
		Log::add( "Inserted temporary translation entry $request_id", Log::DEBUG, 'webt' );
	}
	/**
     * Updates the response of an existing translation entry in the temporary database table.
     *
     * @param string $request_id The request ID associated with the translation entry.
     * @param string $base64Translation The translated text in base64 encoding to be updated in the translation entry.
     * @return void
     */
	public function updateTranslationEntry( $request_id, string $base64Translation ) {
		Log::add( "Updating temporary translation entry $request_id...", Log::DEBUG, 'webt' );
		$query = $this->_db->getQuery( true );

		$fields = array(
			$this->_db->quoteName( 'response' ) . ' = ' . $this->_db->quote( $base64Translation ),
		);

		$query->update( $this->_db->quoteName( $this->table ) )
			->set( $fields )
			->where( $this->getRequestIdCondition( $request_id ) );

		$this->_db->setQuery( $query );
		$this->_db->execute();
		Log::add( "Updated temporary translation entry $request_id.", Log::DEBUG, 'webt' );
	}
	/**
     * Deletes a translation entry from the temporary database table based on the provided request ID.
     *
     * @param string $request_id The request ID to delete the translation entry for.
     * @return void
     */
	public function deleteTranslationEntry( $request_id ) {
		Log::add( "Deleting temporary translation entry $request_id...", Log::DEBUG, 'webt' );
		$query = $this->_db->getQuery( true );

		$query->delete( $this->_db->quoteName( $this->table ) )
			->where( $this->getRequestIdCondition( $request_id ) );

		$this->_db->setQuery( $query );
		$this->_db->execute();
		Log::add( "Deleted temporary translation entry $request_id.", Log::DEBUG, 'webt' );
	}
	/**
     * Creates a condition for the request ID to be used in the SQL query.
     *
     * @param string $request_id The request ID to create the condition for.
     * @return string The condition string.
     */
	private function getRequestIdCondition( $request_id ) {
		return $this->_db->quoteName( 'request_id' ) . ' = ' . $this->_db->quote( $request_id );
	}
}
