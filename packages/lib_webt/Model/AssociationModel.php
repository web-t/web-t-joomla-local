<?php

namespace Webt\Model;

defined( '_JEXEC' ) or die;

use Joomla\CMS\Log\Log;
use Joomla\CMS\MVC\Model\BaseDatabaseModel;

/**
 * The AssociationModel class is responsible for handling association-related database operations.
 *
 * @license     GNU General Public License version 2 or later, see License.txt
 */
class AssociationModel extends BaseDatabaseModel {

	/** @var $table Association table name */
	private $table           = '#__associations';
	/** @var $article_context Article context name */
	private $article_context = 'com_content.item';
	/**
     * Creates an association for an article in the database.
     *
     * @param int $articleId The ID of the article to create the association for.
     * @param string $associationKey The association key to be associated with the article.
     * @return bool True on success, false on failure.
     */
	public function createArticleAssociation( $articleId, $associationKey ) {
		Log::add( "Adding association to article $articleId...", Log::DEBUG, 'webt' );
		$this->deleteExistingAssociation( $articleId );

		$association          = new \stdClass();
		$association->id      = $articleId;
		$association->context = $this->article_context;
		$association->key     = $associationKey;

		Log::add( "Added association to article $articleId.", Log::DEBUG, 'webt' );
		return $this->_db->insertObject( $this->table, $association );
	}
	/**
     * Deletes the existing association for an article from the database.
     *
     * @param int $articleId The ID of the article to delete the association for.
     * @return bool True on success, false on failure.
     */
	private function deleteExistingAssociation( $articleId ) {
		Log::add( "Deleting association for article $articleId...", Log::DEBUG, 'webt' );
		$query = $this->_db->getQuery( true );

		$conditions = array(
			$this->_db->quoteName( 'id' ) . " = $articleId",
			$this->_db->quoteName( 'context' ) . ' = ' . $this->_db->quote( $this->article_context ),
		);

		$query->delete( $this->_db->quoteName( $this->table ) )
			->where( $conditions, 'AND' );
		$this->_db->setQuery( $query );
		Log::add( "Deleted association to article $articleId.", Log::DEBUG, 'webt' );
		return $this->_db->execute();
	}
	/**
     * Retrieves the association for an article from the database.
     *
     * @param int $articleId The ID of the article to get the association for.
     * @return object|null The association object or null if not found.
     */
	public function getArticleAssociation( $articleId ) {
		$query = $this->_db->getQuery( true );

		$conditions = array(
			$this->_db->quoteName( 'id' ) . " = $articleId",
			$this->_db->quoteName( 'context' ) . ' = ' . $this->_db->quote( $this->article_context ),
		);

		$query
			->select( '*' )
			->from( $this->_db->quoteName( $this->table ) )
			->where( $conditions, 'AND' );

		$this->_db->setQuery( $query );

		return $this->_db->loadObject();
	}
}
