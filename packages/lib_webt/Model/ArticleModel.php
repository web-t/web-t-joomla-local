<?php

namespace Webt\Model;

defined( '_JEXEC' ) or die;

use Joomla\CMS\Factory;
use Joomla\CMS\Log\Log;
use Joomla\CMS\MVC\Model\BaseDatabaseModel;

/**
 * The ArticleModel class is responsible for handling article-related database operations.
 * 
 * @license     GNU General Public License version 2 or later, see License.txt
 */
class ArticleModel extends BaseDatabaseModel {
	/**
	 * Retrieves an article by its ID from the database.
	 *
	 * @param int $id The ID of the article.
	 * @return object|null The article object or null if not found.
	 */
	public function getArticleById( $id ) {
		$query = $this->_db->getQuery( true );

		$query
			->select( '*' )
			->from( $this->_db->quoteName( '#__content' ) )
			->where( $this->_db->quoteName( 'id' ) . " = $id" );

		$this->_db->setQuery( $query );

		return $this->_db->loadObject();
	}
	/**
	 * Retrieves all articles in a specific language from the database.
	 *
	 * @param string $language The language code for the articles.
	 * @return array An array of article objects.
	 */
	public function getArticles( string $language ) {
		$query    = $this->_db->getQuery( true );
		$language = $this->_db->escape( $language );

		$conditions = array(
			$this->_db->quoteName( 'a.language' ) . ' = ' . $this->_db->quote( $language ),
			$this->_db->quoteName( 'state' ) . ' != ' . -2,
		);

		$query
			->select( '*' )
			->from( $this->_db->quoteName( '#__content', 'a' ) )
			->where( $conditions, 'AND' );

		$this->_db->setQuery( $query );

		return $this->_db->loadObjectList();
	}
	/**
	 * Retrieves articles with the same alias and language code from the database.
	 *
	 * @param string $title The title of the article.
	 * @param string $targetLanguage The target language code for the articles.
	 * @return array An array of article objects.
	 */
	private function getArticlesWithSameAlias( string $title, string $targetLanguage ):array {
		// Removes perethesis, replaces spaces with dashes and converts to lowercase
		$alias = $this->getAlias( $title, 0 );

		$query = $this->_db->getQuery( true );

		$fields = array(
			$this->_db->quoteName( 'a.alias' ) . ' = ' . $this->_db->quote( '%' . $alias . '%' ),
			$this->_db->quoteName( 'a.language' ) . ' = ' . $this->_db->quote( $targetLanguage ),
		);

		$query
			->select( '*' )
			->from( $this->_db->quoteName( '#__content', 'a' ) )
			->where( $fields );

		$this->_db->setQuery( $query );
		return $this->_db->loadObjectList();
	}
	/**
	 * Retrieves articles with the same alias and language code from the database.
	 *
	 * @param string $title The title of the article.
	 * @param string $targetLanguageCode The target language code for the articles.
	 * @return array An array of article objects.
	 */
	private function getArticlesWithSameAliasAndLanguageCode( string $title, string $targetLanguageCode ):array {
		// Removes perethesis, replaces spaces with dashes and converts to lowercase
		$alias = $this->getAlias( $title, 0, $targetLanguageCode );

		$query = $this->_db->getQuery( true );

		$query
			->select( '*' )
			->from( $this->_db->quoteName( '#__content', 'a' ) )
			->where( $this->_db->quoteName( 'a.alias' ) . ' LIKE ' . $this->_db->quote( '%' . $alias . '%' ) );

		$this->_db->setQuery( $query );
		return $this->_db->loadObjectList();
	}
	/**
	 * Inserts a new translated article into the database.
	 *
	 * @param string $categoryId The ID of the category for the article.
	 * @param string $targetLanguage The target language code for the article.
	 * @param array $translatedText An array containing the translated text for the article.
	 * @param object $sourceArticle The source article object to inherit properties from.
	 * @return int The ID of the created article.
	 * @throws \Exception If the article cannot be inserted.
	 */
	public function insertArticle( string $categoryId, string $targetLanguage, array $translatedText, $sourceArticle ) {
		Log::add( "Inserting article translation [language=$targetLanguage] for source article: $sourceArticle->id...", Log::DEBUG, 'webt' );
		$app          = Factory::getApplication();
		$mvcFactory   = $app->bootComponent( 'com_content' )->getMVCFactory();
		$articleModel = $mvcFactory->createModel( 'Article', 'Administrator', array( 'ignore_request' => true ) );

		$settingsModel     = new SettingsModel();
		$inheritProperties = $settingsModel->getInheritArticleProperties();

		// Title cannot be null
		$title = $translatedText[0];
		if ( $title == null ) {
			$id = $sourceArticle->id;
			Log::add( "Could not insert article: title cannot be null [ID= $id language=$targetLanguage]" , Log::ERROR, 'Article' );
			throw new \Exception( "Could not insert article: invalid translation received! [ID=$id, language=$targetLanguage]" );
		}
		$articles = $this->getArticlesWithSameAlias( $title, $targetLanguage );
		$alias    = $this->getAlias( $title, count( $articles ) );

		// For some reason metadata is serialized to null when called from onContentPrepare event and saving to DB, so we encode metadata as JSON string in advance.
		$metadata = trim( stripslashes( json_encode( $sourceArticle->metadata ) ), '"' );

		$article = array(
			'catid'     => $categoryId,
			'alias'     => $alias,
			'title'     => $title,
			'introtext' => $translatedText[1],
			'fulltext'  => $translatedText[2],
			'state'     => $inheritProperties ? $sourceArticle->state : 0,
			'featured'  => $inheritProperties ? $sourceArticle->featured : 0,
			'language'  => $targetLanguage,
			'images'    => $sourceArticle->images,
			'urls'      => $sourceArticle->urls,
			'attribs'   => $sourceArticle->attribs,
			'metakey'   => $translatedText[3],
			'metadesc'  => $translatedText[4],
			'metadata'  => $metadata,
		);

		if ( ! $articleModel->save( $article ) ) {
			$targetLanguageCode = explode( '-', $targetLanguage )[0];

			$articles            = $this->getArticlesWithSameAliasAndLanguageCode( $title, $targetLanguageCode );
			$alias               = $this->getAlias( $title, count( $articles ), $targetLanguageCode );
			$article['alias']    = $alias;
			$article['language'] = $targetLanguage;

			if ( ! $articleModel->save( $article ) ) {
				Log::add( "Article with an alias ' . $alias . ' and title ' . $title . ' failed to be created.", Log::ERROR, 'webt' );
				throw new \Exception( $articleModel->getError() . ' Article with an alias ' . $alias . ' and title ' . $title . ' failed to be created.' );
			}
		}
		$insertedArticleId = $this->getCreatedArticleId();
		Log::add( "Inserted a new article [ID=$insertedArticleId language=$targetLanguage]", Log::DEBUG, 'webt' );
		return $insertedArticleId;
	}
	/**
	 * Generates an alias for an article title.
	 *
	 * @param string $title The title of the article.
	 * @param int $count The count of articles with the same alias.
	 * @param string $language The language code for the articles.
	 * @return string The generated alias.
	 * @throws \Exception If the PHP Internationalization extension (intl) is not available.
	 */
	private function getAlias( string $title, int $count, string $language = '' ) {
		// Replace spaces with dashes and remove parenthesis
		$alias = strtolower( str_replace( ' ', '-', str_replace( array( '(', ')' ), '', $title ) ) );

		if ( extension_loaded( 'intl' ) ) {
			$rules = <<<'RULES'
				:: Any-Latin;
				:: NFD;
				:: [:Nonspacing Mark:] Remove;
				:: NFC;
				:: [^-[:^Punctuation:]] Remove;
				:: Lower();
				[:^L:] { [-] > ;
				[-] } [:^L:] > ;
				[-[:Separator:]]+ > '-';
			RULES;
			$alias = \Transliterator::createFromRules( $rules )->transliterate( $alias );
		} else {
			Log::add( "Couldn't create an alias from title $title.", Log::ERROR, 'webt' );
			throw new \Exception( 'PHP Internationalization extension (intl) not available. Please enable it in your PHP configuration!' );
		}
		$lang_str = $language ? "-$language" : '';
		if ( $count == 0 ) {
			return $alias . $lang_str;
		} else {
			return $alias . $lang_str . '-' . $count + 1;
		}
	}
	/**
	 * Updates an existing translated article in the database.
	 *
	 * @param int $articleId The ID of the article to be updated.
	 * @param array $translatedText An array containing the translated text for the article.
	 * @param object $sourceArticle The source article object to inherit properties from.
	 */
	public function updateArticle( int $articleId, array $translatedText, $sourceArticle ) {
		Log::add( "Updating article $articleId...", Log::DEBUG, 'webt' );
		$fields = array(
			$this->_db->quoteName( 'title' ) . ' = ' . $this->_db->quote( $translatedText[0] ),
			$this->_db->quoteName( 'fulltext' ) . ' = ' . $this->_db->quote( $translatedText[2] ),
			$this->_db->quoteName( 'introtext' ) . ' = ' . $this->_db->quote( $translatedText[1] ),
			$this->_db->quoteName( 'images' ) . ' = ' . $this->_db->quote( $sourceArticle->images ),
			$this->_db->quoteName( 'urls' ) . ' = ' . $this->_db->quote( $sourceArticle->urls ),
			$this->_db->quoteName( 'attribs' ) . ' = ' . $this->_db->quote( $sourceArticle->attribs ),
			$this->_db->quoteName( 'metadata' ) . ' = ' . $this->_db->quote( $sourceArticle->metadata ),
			$this->_db->quoteName( 'metakey' ) . ' = ' . $this->_db->quote( $translatedText[3] ),
			$this->_db->quoteName( 'metadesc' ) . ' = ' . $this->_db->quote( $translatedText[4] ),
		);

		$settingsModel = new SettingsModel();
		if ( $settingsModel->getInheritArticleProperties() ) {
			$fields[] = $this->_db->quoteName( 'state' ) . ' = ' . $this->_db->quote( $sourceArticle->state );
			$fields[] = $this->_db->quoteName( 'featured' ) . ' = ' . $this->_db->quote( $sourceArticle->featured );
		}

		$query = $this->_db->getQuery( true );
		$query
			->update( $this->_db->quoteName( '#__content' ) )
			->set( $fields )
			->where( $this->_db->quoteName( 'id' ) . ' = ' . $this->_db->quote( $articleId ) );
		$this->_db->setQuery( $query );
		$this->_db->execute();
		Log::add( "Updated article $articleId", Log::DEBUG, 'webt' );
	}
	/**
	 * Retrieves the translation of an article for a specific language.
	 *
	 * @param int $sourceArticleId The ID of the source article.
	 * @param string $translationLangcode The language code for the translation.
	 * @return object|null The translated article object or null if not found.
	 */
	public function getArticleTranslation( $sourceArticleId, $translationLangcode ) {
		$subQuery = $this->_db->getQuery( true )
			->select( 'oa.key' )
			->from( $this->_db->quoteName( '#__content', 'oc' ) )
			->join( 'INNER', $this->_db->quoteName( '#__associations', 'oa' ) . ' ON ' .
				$this->_db->quoteName( 'oc.id' ) . ' = ' . $this->_db->quoteName( 'oa.id' ) . ' AND ' .
				$this->_db->quoteName( 'oa.context' ) . ' = ' . $this->_db->quote( 'com_content.item' )
			)
			->where( $this->_db->quoteName( 'oc.id' ) . " = $sourceArticleId" )
			->setLimit(1);

		$query = $this->_db->getQuery( true )
			->select( 'tc.*' )
			->from( $this->_db->quoteName( '#__content', 'tc' ) )
			->join( 'INNER', $this->_db->quoteName( '#__associations', 'ta' ) . ' ON ' .
				$this->_db->quoteName( 'tc.id' ) . ' = ' . $this->_db->quoteName( 'ta.id' ) . ' AND ' .
				$this->_db->quoteName( 'ta.context' ) . ' = ' . $this->_db->quote( 'com_content.item' )
			)
			->where( $this->_db->quoteName( 'tc.language' ) . ' = ' . $this->_db->quote( $translationLangcode ) )
			->where( $this->_db->quoteName( 'ta.key' ) . ' = (' . $subQuery . ')' )
			->setLimit(1);

		$this->_db->setQuery( $query );

		return $this->_db->loadObject();
	}
	/**
	 * Retrieves the ID of the last created article in the database.
	 *
	 * @return int The ID of the last created article.
	 */
	private function getCreatedArticleId() {
		$query = $this->_db->getQuery( true )
			->select( 'MAX(' . $this->_db->quoteName( 'id' ) . ')' )
			->from( $this->_db->quoteName( '#__content' ) );
		$this->_db->setQuery( $query );
		$maxArticleId = $this->_db->loadResult();

		return $maxArticleId;
	}

}
