<?php

namespace Webt\Model;

defined( '_JEXEC' ) or die;

use Joomla\CMS\Component\ComponentHelper;
use Joomla\CMS\Factory;
use Joomla\CMS\MVC\Model\BaseDatabaseModel;
use Joomla\CMS\Language\LanguageHelper;
use Joomla\Database\DatabaseDriver;
use Joomla\CMS\Log\Log;
use stdClass;

/**
 * The SettingsModel class is responsible for handling settings-related database operations.
 *
 * @license     GNU General Public License version 2 or later, see License.txt
 */
class SettingsModel extends BaseDatabaseModel {

	/**
	 * Available languages user can select to translate to.
	 *
	 * @var array
	 */

	protected $availableLanguages;

	/**
	 * The default language of the site.
	 *
	 * @var string
	 */
	protected $defaultLanguage;
	/**
	 * Api key to use to login MT system.
	 *
	 * @var string
	 */
	protected $apiKey;
	/**
	 * MT system url.
	 *
	 * @var string
	 */
	protected $MTUrl;
	protected DatabaseDriver $db;
	/**
	 * Constructor method that initializes the database driver and retrieves necessary settings.
	 */
	public function __construct() {
		$this->db                 = Factory::getContainer()->get( 'DatabaseDriver' );
		$this->availableLanguages = $this->getLanguages();
		$this->defaultLanguage    = $this->getDefaultLanguage();
		$this->apiKey             = $this->getApiKey();
		$this->MTUrl              = $this->getUrl();
	}
	/**
	 * Retrieves the available languages that the user can select to translate to.
	 * Filters out the default language from the list.
	 *
	 * @return array An array of stdClass objects containing language code and title.
	 */
	public function getLanguages() {
		$langHelper      = new LanguageHelper();
		$languages       = $langHelper->getContentLanguages( array( 1 ), false );
		$languageArray   = array();
		$defaultLanguage = $this->getDefaultLanguage();

		foreach ( $languages as $language ) {
			if ( $language->lang_code == $defaultLanguage ) {
				continue;
			}
			$newObject            = new stdClass();
			$newObject->lang_code = $language->lang_code;
			$newObject->title     = $language->title;
			array_push( $languageArray, $newObject );
		}
		return $languageArray;
	}

	/**
	 * Retrieves the default language of the site from Joomla component parameters.
	 *
	 * @return string The default language code.
	 */
	public function getDefaultLanguage():string {
		$language = ComponentHelper::getParams( 'com_languages' )->get( 'site' );
		return $language;
	}
	/**
	 * Retrieves the MT system URL from the database settings.
	 *
	 * @return string The MT system URL.
	 */
	public function getUrl() {
		return $this->getKeyValue( 'url' );
	}
	/**
	 * Retrieves the API key from the database settings.
	 *
	 * @return string The API key.
	 */
	public function getApiKey() {
		return $this->getKeyValue( 'apikey' );
	}
	/**
	 * Retrieves the MT engine from the database settings.
	 *
	 * @return string The MT engine.
	 */
	public function getMtEngine() {
		return $this->getKeyValue( 'mtEngine', 'etranslation' );
	}
	/**
	 * Retrieves the Etranslation application name from the database settings.
	 *
	 * @return string The Etranslation application name.
	 */
    public function getEtranslationApplicationName() {
		return $this->getKeyValue( 'etranslationApplicationName' );
	}
	/**
	 * Retrieves the Etranslation password from the database settings.
	 *
	 * @return string The Etranslation password.
	 */
    public function getEtranslationPassword() {
		return $this->getKeyValue( 'etranslationPassword' );
	}
	/**
	 * Retrieves the Etranslation timeout from the database settings.
	 *
	 * @return string The Etranslation timeout.
	 */
	public function getEtranslationTimeout() {
		return $this->getKeyValue( 'etranslationTimeout', 10 );
	}
	/**
	 * Retrieves the inherited article property check from the database settings.
	 *
	 * @return string The inherited article property.
	 */
	public function getInheritArticleProperties() {
		return $this->getKeyValue( 'inheritArticleProperties', true );
	}
	/**
	 * Retrieves the translate of visit check from the database settings.
	 *
	 * @return string The translate of visit.
	 */
	public function getTranslateOnVisit() {
		return $this->getKeyValue( 'translateOnVisit' );
	}
	/**
	 * Retrieves the MT notice value from the database settings.
	 *
	 * @return string The MT notice value.
	 */
	public function getShowMtNotice() {
		return $this->getKeyValue( 'showMtNotice', true );
	}
	/**
	 * Retrieves the supported engines from the database settings.
	 *
	 * @return string The supported engines.
	 */
	public function getSupportedEngines() {
		$value = $this->getKeyValue( 'mtEngines' );
		return $value ? json_decode( stripslashes( $value ) ) : null;
	}
	/**
	 * Retrieves the selected engines from the database settings.
	 *
	 * @return string The selected engines.
	 */
	public function getSelectedEngines() {
		$value = $this->getKeyValue( 'selectedEngines' );
		return $value ? json_decode( stripslashes( $value ) ) : null;
	}
	/**
	 * Helper method to retrieve a specific setting entry from the database based on the given key.
	 *
	 * @param string $key The key of the setting to retrieve.
	 * @return object|null The setting entry as an object, or null if not found.
	 */
	private function getSetting( string $key ) {
		$query = $this->db->getQuery( true );
		$key   = $this->db->escape( $key );

		// Select the required fields from the table.
		$query
			->select( '*' )
			->from( $this->db->quoteName( '#__webttranslator_details' ) )
			->where( $this->db->quoteName( 'key' ) . ' = ' . $this->db->quote( $key ) );
		$this->db->setQuery( $query );
		$list = $this->db->loadObjectList();

		// Return empty string if there is no key value in the database table
		if ( empty( $list ) ) {
			return null;
		} else {
			return $list[0];
		}
	}
	/**
	 * Helper method that fetches the value of a specific setting from the database table.
	 * If the setting is not found, it returns the default value provided.
	 *
	 * @param string $key The key of the setting to retrieve.
	 * @param mixed $default_value The default value to return if the setting is not found.
	 * @return string The value of the setting or the default value.
	 */
	private function getKeyValue( string $key, $default_value = '' ):string {
		$settingsObject = $this->getSetting( $key );
		if ( $settingsObject ) {
			return $settingsObject->value;
		}
		return $default_value;
	}
	/**
	 * Saves the MT system url in the database settings.
	 */
	public function setUrl( string $value ) {
		$this->saveConfiguration( 'url', $value );
	}
	/**
	 * Saves the api key in the database settings.
	 */
	public function setApiKey( string $value ) {
		$this->saveConfiguration( 'apikey', $value );
	}
	/**
	 * Saves the MT engines in the database settings.
	 */
	public function setMtEngine( string $value ) {
		$this->saveConfiguration( 'mtEngine', $value );
	}
	/**
	 * Saves the Etranslation application name in the database settings.
	 */
    public function setEtranslationApplicationName( string $value ) {
		return $this->saveConfiguration( 'etranslationApplicationName', $value );
	}
	/**
	 * Saves the Etranslation password in the database settings.
	 */
    public function setEtranslationPassword( string $value ) {
		return $this->saveConfiguration( 'etranslationPassword', $value );
	}
	/**
	 * Saves the Etranslation timeout in the database settings.
	 */
	public function setEtranslationTimeout( string $value ) {
		return $this->saveConfiguration( 'etranslationTimeout', $value );
	}
	/**
	 * Saves the inherited article properties check in the database settings.
	 */
	public function setInheritArticleProperties( string $value ) {
		$this->saveConfiguration( 'inheritArticleProperties', $value );
	}
	/**
	 * Saves the translate on visit check in the database settings.
	 */
	public function setTranslateOnVisit( string $value ) {
		$this->saveConfiguration( 'translateOnVisit', $value );
	}
	/**
	 * Saves the MT notice check in the database settings.
	 */
	public function setShowMtNotice( string $value ) {
		$this->saveConfiguration( 'showMtNotice', $value );
	}
	/**
	 * Saves the supported engines in the database settings.
	 */
	public function setSupportedEngines( $value ) {
		$this->saveConfiguration( 'mtEngines', json_encode( $value ) );
	}
	/**
	 * Saves the selected engines in the database settings.
	 */
	public function setSelectedEngines( $value ) {
		$this->saveConfiguration( 'selectedEngines', json_encode( $value ) );
	}
	/**
	 * Private helper method that handles the saving of configuration settings in the database table.
	 * It updates the existing record if the key exists or inserts a new record if the key is not found.
	 *
	 * @param string $key The key of the setting to save.
	 * @param string $value The value to save for the setting.
	 * @return void
	 */
	private function saveConfiguration( string $key, string $value ):void {
		Log::add( "Saving setting $key...", Log::DEBUG, 'webt' );
		$value     = $this->db->escape( $value );
		$newObject = $this->getSetting( $key );
		if ( $newObject == '' ) {
			$newObject        = new stdClass();
			$newObject->id    = 0;
			$newObject->key   = $key;
			$newObject->value = $value;

			$this->db->insertObject( '#__webttranslator_details', $newObject, 'id' );
			Log::add( "Saved setting $key.", Log::DEBUG, 'webt' );
		} else {
			$newObject->value = $value;

			$this->db->updateObject( '#__webttranslator_details', $newObject, 'id' );
			Log::add( "Updated setting $key.", Log::DEBUG, 'webt' );
		}
	}
}
