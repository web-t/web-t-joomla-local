<?php

defined( '_JEXEC' ) or die;

use Joomla\CMS\Plugin\CMSPlugin;
use Joomla\CMS\Router\ApiRouter;
use Joomla\CMS\Factory;
use Joomla\CMS\Log\Log;

/**
 * Joomla plugin for handling API routing for the eTranslation component in the Webt extension.
 *
 * @license     GNU General Public License version 2 or later, see License.txt
 */
class PlgWebservicesWebt_etranslation extends CMSPlugin {

	/**
	 * Event handler for the `onBeforeApiRoute` event.
	 * Called before the API routing is processed.
	 *
	 * @param ApiRouter $router The API router object.
	 * @return void
	 */
	public function onBeforeApiRoute( ApiRouter &$router ) {
		// Set Accept header, as it is required by Joomla.
		$app           = Factory::getApplication();
		$request       = $app->input;
		$accept_header = 'application/json';

		if ( $request->getMethod() === 'POST' && str_starts_with( $request->server->get( 'PATH_INFO', '', 'html' ), '/v1/webtetranslation/' ) ) 
		{
			$request->server->set( 'HTTP_ACCEPT', $accept_header );
			Log::add( "Received a POST request on eTranslation response endpoint", Log::DEBUG, 'webt' );
		}
		// Define public eTranslation response endpoint.
		$router->createCRUDRoutes(
			'v1/webtetranslation/:id',
			'webtetranslation',
			array(
				'component' => 'com_webt',
				'public'    => true,
				'format'    => array( $accept_header ),
				'id'        => '(^\w+)',
			)
		);
	}
}
