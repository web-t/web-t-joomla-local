<?php

defined( '_JEXEC' ) or die;

use Joomla\CMS\Factory;
use Joomla\CMS\Log\Log;
use Joomla\CMS\Plugin\CMSPlugin;
use Joomla\CMS\Uri\Uri;
use Joomla\CMS\Language\Text;

JLoader::registerNamespace( 'Webt', JPATH_LIBRARIES . '/Webt' );

use Webt\Model\SettingsModel;
use Webt\Model\AssociationModel;
use Webt\Model\ArticleModel;
use Webt\Model\EtranslationResponseModel;
use Webt\Services\EtranslationUtils;
use Webt\Services\TranslationManager;
/**
 * @copyright   Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license     GNU General Public License version 2 or later, see License.txt
 */

// Check to ensure this file is included in Joomla!
defined( '_JEXEC' ) or die( 'Restricted access' );


/**
 * Joomla plugin for managing article translations using the Webt component.
 */
class plgContentWebt extends CMSPlugin {

	/**
	 * Load the language file on instantiation.
	 *
	 * @var    boolean
	 * @since  3.1
	 */
	protected $autoloadLanguage = true;

	// Message keys
	private $schedule_association_creation_key = 'webt_scheduled_associations';
	private $article_changed_key               = 'webt_article_changed';
	/**
	 * Original text used in MT notice. Used only to check if it has been overriden, actual text displayed is retrieved from ini file.
	 *
	 * @var string
	 */
	private $original_notice_text = 'This page has been machine-translated.';

	/**
	 * Method that happens before content is saved
	 * Checks if there is a new translation for an article
	 *
	 * Method is called right before content is saved into the database.
	 * Article object is passed by reference, so any changes will be saved!
	 * NOTE:  Returning false will abort the save with an error.
	 *  You can set the error by calling $article->setError($message)
	 *
	 * @param   object      A JTableContent object
	 * @param   bool        If the content is just about to be created
	 * @return  bool        If false, abort the save
	 */
	function onContentBeforeSave( $context, &$article, $isNew, $data ) {
		$settingsModel  = new SettingsModel();
		$sourceLanguage = $settingsModel->getDefaultLanguage();
		if ( ! $isNew && $context == 'com_content.article' && $article->language == $sourceLanguage && $article->state != -2 ) {
			$articleModel      = new ArticleModel();
			$previous_version  = $articleModel->getArticleById( $article->id );
			$title_changed     = $article->title !== $previous_version->title;
			$fulltext_changed  = $article->fulltext !== $previous_version->fulltext;
			$introtext_changed = $article->introtext !== $previous_version->introtext;
			if ( $title_changed || $fulltext_changed || $introtext_changed ) {
				// Mark article as changed.
				$session = Factory::getSession();
				$session->set( $this->article_changed_key, $article->id );
				Log::add( "Article $article->id has been changed and is marked for retranslation.", Log::DEBUG, 'webt' );
			}
		}
		return true;
	}
	/**
	 * Event handler for the `onContentAfterSave` event.
	 * Called after the content is saved into the database.
	 *
	 * @param string $context Component context.
	 * @param object $article A reference to the saved article.
	 * @param bool   $isNew True if the article is newly created.
	 * @return bool Return true to continue processing or false to halt.
	 */
	function onContentAfterSave( $context, &$article, $isNew ) {
		$settingsModel  = new SettingsModel();
		$sourceLanguage = $settingsModel->getDefaultLanguage();

		if ( $context == 'com_content.article' && $article->language == $sourceLanguage && $article->state != -2 ) {
			$session = Factory::getSession();
			if ( ! $isNew ) {
				$changed_article_id = $session->get( $this->article_changed_key );
				$session->clear( $this->article_changed_key );
				if ( ! $changed_article_id || $changed_article_id !== $article->id ) {
					// Article translatable content not changed, do not retranslate.
					Log::add( "Article [ID = $article->id] content has not changed, skipping retranslation.", Log::DEBUG, 'webt' );
					return;
				}
			}
			$targetLanguageArray = $settingsModel->getLanguages();
			$languageCodeArray   = array_column( $targetLanguageArray, 'lang_code' );

			$key = array_search( $sourceLanguage, $languageCodeArray );
			if ( $key !== false ) {   // Remove source language
				unset( $targetLanguageArray[ $key ] );
			}

			Log::add( "Translating updated source article on save [ID=$article->id]", Log::DEBUG, 'webt' );
			$translationManager = new TranslationManager();
			$new_articles       = $translationManager->translateArticle( $article, $languageCodeArray, true, false );

			// For some reason Joomla deletes article associations created during onContentAfterSave event, so we schedule association creation in a later event.
			$session->set( $this->schedule_association_creation_key, $new_articles );

			Factory::getApplication()->enqueueMessage( 'Article translated!' );
		}
		return true;
	}

	/**
	 * Listener for the `onAfterDispatch` event
	 *
	 * @return  void
	 *
	 * @since   1.0
	 */
	public function onAfterDispatch() {
		// Create translated article associations.
		$session      = Factory::getSession();
		$new_articles = $session->get( $this->schedule_association_creation_key );

		if ( $new_articles !== null ) {
			$associationModel = new AssociationModel();
			foreach ( $new_articles as $key => $values ) {
				foreach ( $values as $articleId ) {
					Log::add( "Adding associations for article $articleId", Log::DEBUG, 'webt' );
					$associationModel->createArticleAssociation( $articleId, $key );
				}
			}
			$session->clear( $this->schedule_association_creation_key );
		}
	}

	/**
	 * Listener for the `onContentPrepare` event.
	 *
	 * @param string  $context Component name and view or name of module.
	 * @param mixed   $article A reference to the article that is being rendered by the view.
	 * @param mixed   $params A reference to an associative array of relevant parameters.
	 * @param integer $page An integer that determines the "page" of the content that is to be generated.
	 * @return void
	 */
	public function onContentPrepare( $context, &$article, &$params, $page ) {
		if ( 'com_content' === explode( '.', $context )[0] && Factory::getApplication()->isClient( 'site' ) ) {
			$this->addMtNotice();
			if ($page === null)
			{
				Log::add( "Checking if article $article->id has translations...", Log::DEBUG, 'webt' );
				$this->checkTranslations( $article );
			}
		}
	}

	/**
	 * Adds MT notice disclaimer.
	 *
	 * @return void
	 */
	private function addMtNotice() {
		$text          = Text::_( 'MT_NOTICE_TEXT' );
		$settingsModel = new SettingsModel();

		if ( $text && $settingsModel->getShowMtNotice() ) {
			$sourceLanguage  = $settingsModel->getDefaultLanguage();
			$currentLanguage = Factory::getLanguage()->getTag();

			if ( $sourceLanguage !== $currentLanguage ) {
				$isNoticeTranslated = $text !== $this->original_notice_text;
				$document           = Factory::getDocument();
				$asset_dir          = Uri::root() . 'administrator/components/com_webt/assets';
				$document->addScriptOptions(
					'webt',
					array(
						'mt_notice_text' => $text,
						'mt_notice_lang' => $isNoticeTranslated ? $currentLanguage : $sourceLanguage,
					)
				);
				$document->addScript( "$asset_dir/js/mt_notice.js" );
				$document->addStyleSheet( "$asset_dir/css/mt_notice.css" );
			}
		}
	}

	/**
	 * Checks if article has all translations and translates to missing languages
	 *
	 * @param mixed $article Article to be loaded.
	 * @return void
	 */
	private function checkTranslations( $article ) {
		$settingsModel = new SettingsModel();
		if ( $settingsModel->getTranslateOnVisit() ) {
			$articleModel   = new ArticleModel();
			$sourceLanguage = $settingsModel->getDefaultLanguage();
			$sourceArticle  = $article;
			if ( $article->language !== $sourceLanguage ) {
				$sourceArticle = $articleModel->getArticleTranslation( $article->id, $sourceLanguage );
			}
			if ( $sourceArticle ) {
				$targetLanguageArray = $settingsModel->getLanguages();
				$languageCodeArray   = array_column( $targetLanguageArray, 'lang_code' );

				$missingTranslations = array();
				foreach ( $languageCodeArray as $langCode ) {
					if ( $langCode === $article->language ) {
						continue;
					}
					if ( ! $articleModel->getArticleTranslation( $sourceArticle->id, $langCode ) && ! $this->checkLateTranslation( $sourceArticle->id, $langCode ) ) {
						Log::add( "Article is missing '$langCode' translation [ID=$sourceArticle->id]", Log::DEBUG, 'webt' );
						$missingTranslations[] = $langCode;
					}
				}

				//If there are missing translations, log it
				if (!empty($missingTranslations))
				{
					$stringLanguages = implode(', ', $missingTranslations);
					Log::add( "Translating article on page load to missing languages ($stringLanguages) [ID=$sourceArticle->id]", Log::INFO, 'webt' );
				}
				$translationManager = new TranslationManager();
				$translationManager->translateArticle( $sourceArticle, $missingTranslations, false, true, false );
			}
		}
	}
	/**
	 * Checks if there is a late translation and applies it to the article.
	 *
	 * @param int    $article_id The ID of the article to check for late translation.
	 * @param string $langcode The language code to check for late translation.
	 * @return bool Returns true if a late translation was applied, false otherwise.
	 */
	private function checkLateTranslation( $article_id, $langcode ) {
		$settingsModel = new SettingsModel();
		if ( 'etranslation' === $settingsModel->getMtEngine() ) {
			$responseModel  = new EtranslationResponseModel();
			$entry          = $responseModel->getTranslationEntryByArticleIdAndLangcode( $article_id, $langcode );
			if (!$entry)
			{
				return false;
			}
			$is_translation = ! in_array( $entry->response, array( EtranslationUtils::$initial_value, EtranslationUtils::$timeout_value, EtranslationUtils::$error_value ), true );
			if ( $entry && $is_translation ) {
				Log::add( "Found late eTranslation response entry for article [ID=$article_id]", Log::INFO, 'webt' );
				$translations       = EtranslationUtils::decode_response( $entry->response );
				$translationManager = new TranslationManager();
				$translationManager->translateArticleFromTextTranslation( $article_id, $langcode, $translations );
				$responseModel->deleteTranslationEntry( $responseModel->request_id );
				return true;
			}
		}
		return false;
	}
}
